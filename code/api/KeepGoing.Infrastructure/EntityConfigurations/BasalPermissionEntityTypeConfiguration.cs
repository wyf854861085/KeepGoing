﻿using Girvs.EntityFrameworkCore.EntityConfigurations;
using KeepGoing.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Infrastructure.EntityConfigurations
{
    public class BasalPermissionEntityTypeConfiguration : GirvsAbstractEntityTypeConfiguration<BasalPermission>
    {
        public override void Configure(EntityTypeBuilder<BasalPermission> builder)
        {
            OnModelCreatingBaseEntityAndTableKey<BasalPermission, Guid>(builder);
            builder.Property(x => x.AppliedObjectType).HasColumnType("int");
            builder.Property(x => x.ValidateObjectType).HasColumnType("int");
            builder.Property(x => x.DenyMask).HasColumnType("bigint");
            builder.Property(x => x.AllowMask).HasColumnType("bigint");

            //需要添加系统管理员的权限
            //索引 
            builder.HasIndex(x => x.AppliedObjectType);
            builder.HasIndex(x => x.ValidateObjectType);
            builder.HasIndex(x => x.AppliedID);
            builder.HasIndex(x => x.AppliedObjectID);
            builder.HasIndex(x => x.ValidateObjectID);
        }
    }
}