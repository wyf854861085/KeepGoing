﻿using Girvs.EntityFrameworkCore.Context;
using KeepGoing.Domain.Models;
using KeepGoing.Infrastructure.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Infrastructure
{
    [Girvs.EntityFrameworkCore.DbContextExtensions.GirvsDbConfig("KeepGoing")]
    public class KeepGoingDbContext : GirvsDbContext
    {
        public KeepGoingDbContext(DbContextOptions<KeepGoingDbContext> options) : base(options)
        {
        }
        /// <summary>
        /// 用户
        /// </summary>
        public DbSet<User> Users { get; set; }

        /// <summary>
        /// 授权
        /// </summary>
        public DbSet<BasalPermission> basalPermissions { get; set; }

        /// <summary>
        /// 角色管理
        /// </summary>
        public DbSet<Role> Roles { get; set; }

        public DbSet<ServicePermission> ServicePermissions { get; set; }

        /// <summary>
        /// 自动回复
        /// </summary>
        public DbSet<KeywordReply> keywordReplies { get; set; }

        /// <summary>
        /// 微信消息
        /// </summary>
        public DbSet<WXTextMessage> wXTextMessages { get; set; }

        /// <summary>
        /// 词库
        /// </summary>
        public DbSet<Terms> terms { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ServicePermissionEntityTypeConfiguration());
        }
    }
}
