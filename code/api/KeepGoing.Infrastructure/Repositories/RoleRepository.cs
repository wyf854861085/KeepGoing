﻿using Girvs.BusinessBasis.Queries;
using Girvs.EntityFrameworkCore.Repositories;
using KeepGoing.Domain.Models;
using KeepGoing.Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace KeepGoing.Infrastructure.Repositories
{
    public class RoleRepository : Repository<Role>, IRoleRepository
    {
        public override async Task<List<Role>> GetByQueryAsync(QueryBase<Role> query)
        {
            query.RecordCount = await Queryable.Where(query.GetQueryWhere()).CountAsync();

            if (query.RecordCount < 1)
            {
                query.Result = new List<Role>();
            }
            else
            {
                query.Result =
                    await Queryable.Where(query.GetQueryWhere())
                        .OrderBy(x => x.Sort)
                        .Skip(query.PageStart)
                        .Take(query.PageSize).ToListAsync();
            }
            return query.Result;
        }
    }
}