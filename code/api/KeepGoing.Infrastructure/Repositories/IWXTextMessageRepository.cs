﻿using Girvs.EntityFrameworkCore.Repositories;
using KeepGoing.Domain.Models;
using KeepGoing.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using Org.BouncyCastle.Asn1.Ocsp;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Aliyun.Api.LogService.Infrastructure.Serialization.Protobuf.Log.Types;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace KeepGoing.Infrastructure.Repositories
{
    public class IWXTextMessageRepository : Repository<WXTextMessage>, IIWXTextMessageRepository
    {
        private readonly KeepGoingDbContext _dbContext;

        public IWXTextMessageRepository([NotNull] KeepGoingDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public async Task<int> GetCountByWxId(string? AppId, string? Content)
        {
            return await _dbContext.wXTextMessages.CountAsync(x => x.AppId == AppId && x.Content == Content);
        }


        public async Task<List<WXTextMessage>> GetByWxIdFromUserName(string AppId, string FromUserName)
        {
            var startData = await _dbContext.wXTextMessages.Where(x => x.AppId == AppId && x.FromUserName == FromUserName && (x.Content == "发文")).OrderByDescending(x => x.CreateTime).FirstOrDefaultAsync();

            if (startData == null)
            {
                return new List<WXTextMessage>();
            }
            return await _dbContext.wXTextMessages.Where(x => x.AppId == AppId && x.FromUserName == FromUserName && x.CreateTime > startData.CreateTime).OrderBy(x => x.CreateTime).ToListAsync(); ;
        }

        public async Task<bool> AddWXTextMessage(string AppId, string Content, string ReplyContent, string FromUserName)
        {
            WXTextMessage wXTextMessage = new WXTextMessage
            {
                AppId = AppId,
                Content = Content,
                CreateTime = DateTime.Now,
                FromUserName = FromUserName,
                Id = Guid.NewGuid(),
                Idx = 1,
                ReplyContent = ReplyContent,
                MsgDataId = 1,
                MsgId = 1,
                MsgType = "",
                ToUserName = "",
            };
            await _dbContext.wXTextMessages.AddAsync(wXTextMessage);
            _dbContext.SaveChanges();
            return true;
        }



        public async Task<string?> GetRandText()
        {
            var totalRecords = await _dbContext.terms.CountAsync();
            var randomOffset = new Random().Next(totalRecords);
            var randomRecord = await _dbContext.terms.Skip(randomOffset).Take(1).FirstOrDefaultAsync();

            return randomRecord?.Text;
        }
    }
}
