﻿using Girvs.EntityFrameworkCore.Repositories;
using JetBrains.Annotations;
using KeepGoing.Domain.Models;
using KeepGoing.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Infrastructure.Repositories
{
    internal class PermissionRepository : Repository<BasalPermission>, IPermissionRepository
    {
        private readonly KeepGoingDbContext _dbContext;

        public PermissionRepository([NotNull] KeepGoingDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public Task<List<BasalPermission>> GetUserPermissionLimit(Guid userId)
        {
            return _dbContext.Set<BasalPermission>().Where(x => x.AppliedID == userId).ToListAsync();
        }
    }
}
