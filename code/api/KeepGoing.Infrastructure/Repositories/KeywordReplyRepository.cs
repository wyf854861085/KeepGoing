﻿using Girvs.BusinessBasis.Queries;
using Girvs.EntityFrameworkCore.Repositories;
using KeepGoing.Domain.Models;
using KeepGoing.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Infrastructure.Repositories
{
    public class KeywordReplyRepository : Repository<KeywordReply>, IKeywordReplyRepository
    {
        private readonly KeepGoingDbContext _dbContext;

        public KeywordReplyRepository(KeepGoingDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public override async Task<List<KeywordReply>> GetByQueryAsync(QueryBase<KeywordReply> query)
        {
            query.RecordCount = await Queryable.Where(query.GetQueryWhere()).CountAsync();

            if (query.RecordCount < 1)
            {
                query.Result = new List<KeywordReply>();
            }
            else
            {
                query.Result =
                    await Queryable.Where(query.GetQueryWhere())
                        .OrderByDescending(x => x.CreateTime)
                        .Skip(query.PageStart)
                        .Take(query.PageSize).ToListAsync();
            }
            return query.Result;
        }

        /// <summary>
        /// 获取最新上传的图片
        /// </summary>
        /// <param name="AppId"></param>
        /// <returns></returns>
        public async Task<string?> GetImageFileByAppId(string AppId)
        {
            return (await Queryable.Where(x => x.AppId == AppId && x.imageFile != "default").OrderByDescending(x => x.CreateTime).FirstOrDefaultAsync())?.imageFile;
        }

        public async Task<bool> GetKeywordReplyNameAny(string AppId, string? KeywordReplyName)
        {
            if (!string.IsNullOrEmpty(KeywordReplyName))
            {
                return false;
            }
            return await Queryable.AnyAsync(x => x.AppId == AppId && x.KeywordReplyName == KeywordReplyName);
        }
    }
}
