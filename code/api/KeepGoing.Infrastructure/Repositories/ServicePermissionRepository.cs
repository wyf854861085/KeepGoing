﻿using Girvs.EntityFrameworkCore.Repositories;
using KeepGoing.Domain.Models;
using KeepGoing.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Infrastructure.Repositories
{
    public class ServicePermissionRepository : Repository<ServicePermission, Guid>, IServicePermissionRepository
    {
        public Task<ServicePermission> GetEntityByWhere(Expression<Func<ServicePermission, bool>> expression)
        {
            return GetAsync(expression);
        }
    }
}
