﻿using Girvs.BusinessBasis.Queries;
using Girvs.EntityFrameworkCore.Repositories;
using Girvs.Infrastructure;
using KeepGoing.Domain.Models;
using KeepGoing.Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace KeepGoing.Infrastructure.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public override async Task<List<User>> GetByQueryAsync(QueryBase<User> query)
        {
            query.RecordCount = await Queryable.Where(query.GetQueryWhere()).CountAsync();

            if (query.RecordCount < 1)
            {
                query.Result = new List<User>();
            }
            else
            {
                query.Result =
                    await Queryable.Where(query.GetQueryWhere())
                        .OrderBy(x => x.CreateTime)
                        .Skip(query.PageStart)
                        .Take(query.PageSize).ToListAsync();
            }
            return query.Result;
        }

        public async Task<User?> GetUserByLoginNameAsync(string loginName)
        {
            //此处需地绕过所有的查询条件
            var dbContext = EngineContext.Current.Resolve<KeepGoingDbContext>();
            return await dbContext.Set<User>().FirstOrDefaultAsync(x => x.UserAccount == loginName);
        }

        public async Task<User?> GetUserByRoleAsync(Guid userId)
        {
            return await Queryable.Include(x => x.Roles).FirstOrDefaultAsync(x => x.Id == userId);
        }
    }
}
