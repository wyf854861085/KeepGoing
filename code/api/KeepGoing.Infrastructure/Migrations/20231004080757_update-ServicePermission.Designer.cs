﻿// <auto-generated />
using System;
using KeepGoing.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

#nullable disable

namespace KeepGoing.Infrastructure.Migrations
{
    [DbContext(typeof(KeepGoingDbContext))]
    [Migration("20231004080757_update-ServicePermission")]
    partial class updateServicePermission
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "6.0.8")
                .HasAnnotation("Relational:MaxIdentifierLength", 64);

            modelBuilder.Entity("KeepGoing.Domain.Models.BasalPermission", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("char(36)");

                    b.Property<long>("AllowMask")
                        .HasColumnType("bigint");

                    b.Property<Guid>("AppliedID")
                        .HasColumnType("char(36)");

                    b.Property<Guid>("AppliedObjectID")
                        .HasColumnType("char(36)");

                    b.Property<int>("AppliedObjectType")
                        .HasColumnType("int");

                    b.Property<long>("DenyMask")
                        .HasColumnType("bigint");

                    b.Property<int>("ValidateObjectID")
                        .HasColumnType("int");

                    b.Property<int>("ValidateObjectType")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.ToTable("basalPermissions");
                });

            modelBuilder.Entity("KeepGoing.Domain.Models.Role", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("char(36)");

                    b.Property<string>("RoleDesc")
                        .IsRequired()
                        .HasColumnType("longtext");

                    b.Property<string>("RoleName")
                        .IsRequired()
                        .HasColumnType("longtext");

                    b.Property<int>("Sort")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.ToTable("Role");
                });

            modelBuilder.Entity("KeepGoing.Domain.Models.ServicePermission", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("char(36)");

                    b.Property<long>("FuncModule")
                        .HasColumnType("bigint");

                    b.Property<string>("OperationPermissions")
                        .HasColumnType("text");

                    b.Property<int>("Order")
                        .HasColumnType("int");

                    b.Property<string>("OtherParams")
                        .HasColumnType("text");

                    b.Property<string>("Permissions")
                        .HasColumnType("text");

                    b.Property<string>("ServiceId")
                        .IsRequired()
                        .HasColumnType("varchar(36)");

                    b.Property<string>("ServiceName")
                        .HasColumnType("varchar(255)");

                    b.Property<string>("Tag")
                        .HasColumnType("varchar(50)");

                    b.HasKey("Id");

                    b.ToTable("ServicePermission", (string)null);
                });

            modelBuilder.Entity("KeepGoing.Domain.Models.User", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("char(36)");

                    b.Property<string>("ContactNumber")
                        .HasColumnType("varchar(12)");

                    b.Property<DateTime>("CreateTime")
                        .HasColumnType("datetime(6)");

                    b.Property<Guid>("CreatorId")
                        .HasColumnType("char(36)");

                    b.Property<bool>("IsInitData")
                        .HasColumnType("tinyint(1)");

                    b.Property<Guid>("TenantId")
                        .HasColumnType("char(36)");

                    b.Property<string>("UserAccount")
                        .IsRequired()
                        .HasColumnType("varchar(36)");

                    b.Property<string>("UserName")
                        .IsRequired()
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("UserPassword")
                        .IsRequired()
                        .HasColumnType("varchar(36)");

                    b.Property<int>("UserType")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("CreateTime");

                    b.HasIndex("UserAccount");

                    b.HasIndex("UserName");

                    b.ToTable("Users");

                    b.HasData(
                        new
                        {
                            Id = new Guid("58205e0e-1552-4282-bedc-a92d0afb37df"),
                            CreateTime = new DateTime(2023, 10, 4, 16, 7, 57, 7, DateTimeKind.Local).AddTicks(4377),
                            CreatorId = new Guid("58205e0e-1552-4282-bedc-a92d0afb37df"),
                            IsInitData = true,
                            TenantId = new Guid("00000000-0000-0000-0000-000000000000"),
                            UserAccount = "admin",
                            UserName = "系统管理员",
                            UserPassword = "21232F297A57A5A743894A0E4A801FC3",
                            UserType = 1
                        });
                });
#pragma warning restore 612, 618
        }
    }
}
