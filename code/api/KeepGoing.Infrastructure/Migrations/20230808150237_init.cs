﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace KeepGoing.Infrastructure.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterDatabase()
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    UserAccount = table.Column<string>(type: "varchar(36)", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    UserPassword = table.Column<string>(type: "varchar(36)", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    UserName = table.Column<string>(type: "nvarchar(50)", nullable: false),
                    ContactNumber = table.Column<string>(type: "varchar(12)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    UserType = table.Column<int>(type: "int", nullable: false),
                    IsInitData = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    TenantId = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    CreateTime = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    CreatorId = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "ContactNumber", "CreateTime", "CreatorId", "IsInitData", "TenantId", "UserAccount", "UserName", "UserPassword", "UserType" },
                values: new object[] { new Guid("58205e0e-1552-4282-bedc-a92d0afb37df"), null, new DateTime(2023, 8, 8, 23, 2, 37, 627, DateTimeKind.Local).AddTicks(2640), new Guid("58205e0e-1552-4282-bedc-a92d0afb37df"), true, new Guid("00000000-0000-0000-0000-000000000000"), "admin", "系统管理员", "21232F297A57A5A743894A0E4A801FC3", 1 });

            migrationBuilder.CreateIndex(
                name: "IX_Users_CreateTime",
                table: "Users",
                column: "CreateTime");

            migrationBuilder.CreateIndex(
                name: "IX_Users_UserAccount",
                table: "Users",
                column: "UserAccount");

            migrationBuilder.CreateIndex(
                name: "IX_Users_UserName",
                table: "Users",
                column: "UserName");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
