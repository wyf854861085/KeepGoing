﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace KeepGoing.Infrastructure.Migrations
{
    public partial class WXTextMessage_add_ReplyContent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ReplyContent",
                table: "wXTextMessages",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("58205e0e-1552-4282-bedc-a92d0afb37df"),
                column: "CreateTime",
                value: new DateTime(2025, 1, 3, 10, 5, 50, 644, DateTimeKind.Local).AddTicks(6666));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ReplyContent",
                table: "wXTextMessages");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("58205e0e-1552-4282-bedc-a92d0afb37df"),
                column: "CreateTime",
                value: new DateTime(2025, 1, 3, 9, 52, 14, 866, DateTimeKind.Local).AddTicks(9907));
        }
    }
}
