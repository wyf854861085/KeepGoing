﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace KeepGoing.Infrastructure.Migrations
{
    public partial class updateServicePermission : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Tag",
                table: "ServicePermission",
                type: "varchar(50)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(50)")
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<string>(
                name: "ServiceName",
                table: "ServicePermission",
                type: "varchar(255)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(255)")
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<string>(
                name: "Permissions",
                table: "ServicePermission",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text")
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<string>(
                name: "OtherParams",
                table: "ServicePermission",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text")
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<string>(
                name: "OperationPermissions",
                table: "ServicePermission",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text")
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("58205e0e-1552-4282-bedc-a92d0afb37df"),
                column: "CreateTime",
                value: new DateTime(2023, 10, 4, 16, 7, 57, 7, DateTimeKind.Local).AddTicks(4377));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "ServicePermission",
                keyColumn: "Tag",
                keyValue: null,
                column: "Tag",
                value: "");

            migrationBuilder.AlterColumn<string>(
                name: "Tag",
                table: "ServicePermission",
                type: "varchar(50)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(50)",
                oldNullable: true)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "ServicePermission",
                keyColumn: "ServiceName",
                keyValue: null,
                column: "ServiceName",
                value: "");

            migrationBuilder.AlterColumn<string>(
                name: "ServiceName",
                table: "ServicePermission",
                type: "varchar(255)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(255)",
                oldNullable: true)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "ServicePermission",
                keyColumn: "Permissions",
                keyValue: null,
                column: "Permissions",
                value: "");

            migrationBuilder.AlterColumn<string>(
                name: "Permissions",
                table: "ServicePermission",
                type: "text",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "ServicePermission",
                keyColumn: "OtherParams",
                keyValue: null,
                column: "OtherParams",
                value: "");

            migrationBuilder.AlterColumn<string>(
                name: "OtherParams",
                table: "ServicePermission",
                type: "text",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "ServicePermission",
                keyColumn: "OperationPermissions",
                keyValue: null,
                column: "OperationPermissions",
                value: "");

            migrationBuilder.AlterColumn<string>(
                name: "OperationPermissions",
                table: "ServicePermission",
                type: "text",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true)
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("58205e0e-1552-4282-bedc-a92d0afb37df"),
                column: "CreateTime",
                value: new DateTime(2023, 9, 28, 15, 34, 38, 612, DateTimeKind.Local).AddTicks(8473));
        }
    }
}
