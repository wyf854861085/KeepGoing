﻿using KeepGoing.Application.AppService.WeiXin;
using Microsoft.AspNetCore.SignalR;

namespace KeepGoing.WebApi.Hubs
{
    public class ChatHub : Hub
    {
        private readonly IWeiXinAppService _weiXinAppService;

        public ChatHub(IWeiXinAppService weiXinAppService)
        {
            _weiXinAppService = weiXinAppService;
        }

        public async Task SendMessage(string user, string message)
        {
            var result = await _weiXinAppService.ReceiveMessage(user, message);
            user = "Admin";
            await Clients.All.SendAsync("ReceiveMessage", user, result);
        }
    }
}
