﻿using Girvs.Configuration;
using KeepGoing.Application;

namespace KeepGoing.WebApi.App_Data
{
    public class AppSettingsNew : AppSettings
    {
        public SenparcWeixinSetting SenparcWeixinSetting { get; set; } = new SenparcWeixinSetting();

        public SenparcSetting SenparcSetting { get; set; } = new SenparcSetting();

        public string FileApiHost { get; set; } = "http://192.168.51.123/";
    }
}
