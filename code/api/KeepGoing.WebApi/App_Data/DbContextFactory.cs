﻿using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore;
using KeepGoing.Infrastructure;

namespace KeepGoing.WebApi.App_Data
{
#if DEBUG

    public class DbContextFactory : IDesignTimeDbContextFactory<KeepGoingDbContext>
    {
        public KeepGoingDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<KeepGoingDbContext>();
            optionsBuilder.UseMySql(
                "Data Source=120.77.99.167;Database=KeepGoing;UserID=root;Password=Zhuofan@123??;pooling=true;CharSet=utf8mb4;port=3306;sslmode=none;",
                new MySqlServerVersion(new Version(8, 0, 25)));
            return new KeepGoingDbContext(optionsBuilder.Options);
        }
    }
#endif
}
