﻿using Girvs.Configuration;
using Girvs.FileProvider;
using Girvs.Infrastructure.Extensions;
using Girvs.Infrastructure;
using Girvs;
using Microsoft.Extensions.Options;
using Senparc.CO2NET.Extensions;
using Senparc.CO2NET.HttpUtility;
using Senparc.CO2NET.RegisterServices;
using Senparc.CO2NET.Trace;
using Senparc.Weixin.Entities;
using Senparc.Weixin.Helpers;
using System.Net;
using System.Security.Cryptography.X509Certificates;

namespace KeepGoing.WebApi.App_Data
{
    public static class ServiceCollectionExtensions
    {
        public static (IEngine, AppSettings) ConfigureApplicationServicesNew(this IServiceCollection services,
            IConfiguration configuration, IWebHostEnvironment webHostEnvironment)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.SystemDefault;
            CommonHelper.DefaultFileProvider = new GirvsFileProvider(webHostEnvironment);

            services.AddHttpContextAccessor();

            var appSettings = new AppSettingsNew();
            services.AddBindAppModelConfiguation(configuration, appSettings);
            services.AddBindSerilogConfiguation(configuration);

            var engine = EngineContext.Create();

            engine.ConfigureServices(services, configuration);

            // ServiceContextFactory.Create(services.BuildServiceProvider());
            return (engine, appSettings);
        }

        public static void AddHttpContextAccessor(this IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        }
    }
}
