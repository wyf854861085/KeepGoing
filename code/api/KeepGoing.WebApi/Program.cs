using Girvs.AuthorizePermission.Extensions;
using Girvs.DynamicWebApi;
using Girvs.Infrastructure.Extensions;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.Options;
using Senparc.Weixin.RegisterServices;
using Senparc.Weixin.AspNet;
using Senparc.Weixin.MP;
using KeepGoing.WebApi.App_Data;
using Microsoft.Extensions.FileProviders;
using Serilog;
using static Aliyun.Api.LogService.Infrastructure.Serialization.Protobuf.Log.Types;
using KeepGoing.WebApi.Hubs;
using Microsoft.AspNetCore.SignalR;

var builder = WebApplication.CreateBuilder(args);


//使用本地缓存必须添加
builder.Services.AddMemoryCache();

// Add services to the container.

builder.Services.AddControllersWithAuthorizePermissionFilter(options =>
   options.Filters.Add<GirvsModelStateInvalidFilter>());
builder.Services.Configure<ForwardedHeadersOptions>(options =>
{
    options.ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
    options.KnownNetworks.Clear();
    options.KnownProxies.Clear();
});
builder.Services.AddCors(option =>
{
    option.AddPolicy("AllowSpecificOrigin", policy =>
    {
        policy.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod();
        //policy.WithOrigins("http://localhost:5019/").AllowAnyHeader().AllowAnyMethod();
    });
});

builder.Services.ConfigureApplicationServicesNew(builder.Configuration, builder.Environment);

builder.Services.AddSignalR();

builder.Services.AddSenparcWeixinServices(builder.Configuration);

var app = builder.Build();

var registerService = app.UseSenparcWeixin(app.Environment,
    null /* 不为 null 则覆盖 appsettings  中的 SenpacSetting 配置*/,
    null /* 不为 null 则覆盖 appsettings  中的 SenpacWeixinSetting 配置*/,
    register => { /* CO2NET 全局配置 */ },
    (register, weixinSetting) =>
    {
        //注册公众号信息（可以执行多次，注册多个公众号）
        register.RegisterMpAccount(weixinSetting, "dotnet成长");
        register.RegisterMpAccount(weixinSetting.Items["dotnet成长"], "dotnet成长");
        register.RegisterMpAccount(weixinSetting.Items["小胖吴的小铁匠"], "小胖吴的小铁匠");
    });

app.UseCors("AllowSpecificOrigin");


// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseStaticFiles();

app.UseHttpsRedirection();

app.UseStaticFiles(new StaticFileOptions
{
    FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "fileUpload")),
    RequestPath = "/fileUpload"
});

app.UseGirvsExceptionHandler();

app.UseAuthorization();

app.MapControllers();

app.MapHub<ChatHub>("/api/chatHub");

app.Run();
