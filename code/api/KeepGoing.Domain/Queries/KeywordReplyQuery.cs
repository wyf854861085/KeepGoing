﻿using Girvs.BusinessBasis.Queries;
using Girvs.Extensions;
using KeepGoing.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Domain.Queries
{
    public class KeywordReplyQuery : QueryBase<KeywordReply>
    {
        public KeywordReplyQuery()
        {
            OrderBy = nameof(KeywordReply.CreateTime) + " desc ";
        }

        /// <summary>
        /// 规则名称
        /// </summary>

        [QueryCacheKey]
        public string RuleName { get; set; }

        /// <summary>
        /// 微信AppID
        /// </summary>

        [QueryCacheKey]
        public string AppId { get; set; }

        /// <summary>
        /// 关键字内容
        /// </summary>

        [QueryCacheKey]
        public string KeywordReplyName { get; set; }

        public override Expression<Func<KeywordReply, bool>> GetQueryWhere()
        {
            Expression<Func<KeywordReply, bool>> expression = exam => true;
            if (!string.IsNullOrEmpty(RuleName))
            {
                expression = expression.And(x => x.RuleName.Contains(RuleName)|| x.KeywordReplyName.Contains(RuleName));
            }
            if (!string.IsNullOrEmpty(KeywordReplyName))
            {
                expression = expression.And(x => x.KeywordReplyName.Contains(KeywordReplyName));
            }
            if (!string.IsNullOrEmpty(AppId))
            {
                expression = expression.And(x => x.AppId == AppId);
            }
            return expression;
        }
    }
}
