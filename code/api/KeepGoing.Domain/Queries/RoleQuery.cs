﻿using Girvs.BusinessBasis.Queries;
using Girvs.Extensions;
using System.Linq.Expressions;
using Role = KeepGoing.Domain.Models.Role;

namespace KeepGoing.Domain.Queries
{
    public class RoleQuery : QueryBase<Role>
    {
        public RoleQuery()
        {
            OrderBy = nameof(Role.Sort);
        }

        /// <summary>
        /// 角色名称
        /// </summary>

        [QueryCacheKey]
        public string RoleName { get; set; }

        public override Expression<Func<Role, bool>> GetQueryWhere()
        {
            Expression<Func<Role, bool>> expression = exam => true;
            if (!string.IsNullOrEmpty(RoleName))
            {
                expression = expression.And(x => x.RoleName.Contains(RoleName));
            }
            return expression;
        }
    }
}
