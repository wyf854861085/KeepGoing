﻿
using Girvs.AuthorizePermission.Enumerations;
using Girvs.AuthorizePermission.Extensions;
using Girvs.BusinessBasis.Queries;
using Girvs.Extensions;
using Girvs.Infrastructure;
using KeepGoing.Domain.Models;
using System.Linq.Expressions;

namespace KeepGoing.Domain.Queries
{
    public class UserQuery : QueryBase<User>
    {
        [QueryCacheKey]
        public string UserName { get; set; }

        [QueryCacheKey]
        public string UserAccount { get; set; }


        public override Expression<Func<User, bool>> GetQueryWhere()
        {
            Expression<Func<User, bool>> expression = user => true;

            if (!string.IsNullOrEmpty(UserName))
            {
                expression = expression.And(user => user.UserName.Contains(UserName));
            }

            if (!string.IsNullOrEmpty(UserAccount))
            {
                expression = expression.And(user => user.UserAccount.Contains(UserAccount));
            }

            //如果当前用户是普通用户，则列表只显示当前用户自己创建的用户列表
            var httpContext = EngineContext.Current.HttpContext;
            if (httpContext?.User.Identity != null && httpContext.User.Identity.IsAuthenticated)
            {
                var UserType = EngineContext.Current.ClaimManager.GetUserType();
                var userId = EngineContext.Current.ClaimManager.GetUserId();
                if (UserType == UserType.GeneralUser)
                {
                    expression = expression.And(x => x.CreatorId == userId.ToGuid());
                }
                expression = expression.And(x => x.Id != userId.ToGuid());
            }

            return expression;
        }
    }
}