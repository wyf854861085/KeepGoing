﻿using Girvs.BusinessBasis.UoW;
using Girvs.Cache.Caching;
using Girvs.Driven.Bus;
using Girvs.Driven.CacheDriven.Events;
using Girvs.Driven.Commands;
using Girvs.Driven.Notifications;
using Girvs.Extensions;
using KeepGoing.Domain.Commands.KeywordReply;
using KeepGoing.Domain.Commands.User;
using KeepGoing.Domain.Extensions;
using KeepGoing.Domain.Models;
using KeepGoing.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Domain.CommandHanlders
{
    public class KeywordReplyCommandHandler :
        CommandHandler,
        IRequestHandler<CreateKeywordReplyCommand, bool>,
        IRequestHandler<UpdataKeywordReplyCommand, bool>,
        IRequestHandler<DeleteKeywordReplyCommand, bool>
    {
        private readonly IMediatorHandler _bus;
        private readonly IKeywordReplyRepository _keywordReplyRepository;

        public KeywordReplyCommandHandler(
            IUnitOfWork<KeywordReply> uow,
            IMediatorHandler bus,
            IKeywordReplyRepository keywordReplyRepository) : base(uow, bus)
        {
            _bus = bus ?? throw new ArgumentNullException(nameof(bus));
            _keywordReplyRepository = keywordReplyRepository ?? throw new ArgumentNullException(nameof(keywordReplyRepository));
        }

        /// <summary>
        /// 创建规则
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<bool> Handle(CreateKeywordReplyCommand request, CancellationToken cancellationToken)
        {
            if (await _keywordReplyRepository.ExistEntityAsync(x => x.KeywordReplyName == request.KeywordReplyName && x.AppId == request.AppId))
            {
                await _bus.RaiseEvent(new DomainNotification(request.KeywordReplyName.ToString(), "规则名称已经存在"), cancellationToken);
                return false;
            }
            KeywordReply keywordReply = new KeywordReply
            {
                AppId = request.AppId,
                RuleName = request.RuleName,
                KeywordReplyName = request.KeywordReplyName,
                MateType = request.MateType,
                ReplyContent = request.ReplyContent,
                imageFile = request.imageFile,
                txtFile = request.txtFile,
                CreateTime = DateTime.Now
            };
            await _keywordReplyRepository.AddAsync(keywordReply);
            if (await Commit())
            {
                var key = GirvsEntityCacheDefaults<KeywordReply>.ByIdCacheKey.Create(keywordReply.Id.ToString());

                await _bus.RaiseEvent(new SetCacheEvent(keywordReply, key, key.CacheTime), cancellationToken);
                // 删除查询相关的缓存
                await _bus.RaiseEvent(new RemoveCacheListEvent(GirvsEntityCacheDefaults<KeywordReply>.ListCacheKey.Create()), cancellationToken);
            }
            return true;
        }

        /// <summary>
        /// 修改规则
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<bool> Handle(UpdataKeywordReplyCommand request, CancellationToken cancellationToken)
        {
            KeywordReply keywordReply = await _keywordReplyRepository.GetByIdAsync(request.Id);
            if (keywordReply == null)
            {
                await _bus.RaiseEvent(new DomainNotification(request.Id.ToString(), "修改失败，规则记录不存在"), cancellationToken);
                return false;
            }

            if (request.KeywordReplyName != keywordReply.KeywordReplyName && await _keywordReplyRepository.ExistEntityAsync(x => x.KeywordReplyName == request.KeywordReplyName && x.AppId == keywordReply.AppId))
            {
                await _bus.RaiseEvent(new DomainNotification(request.KeywordReplyName.ToString(), "关键词已经存在"), cancellationToken);
                return false;
            }

            keywordReply.ReplyContent = request.ReplyContent;
            keywordReply.KeywordReplyName = request.KeywordReplyName;
            keywordReply.RuleName = request.RuleName;
            keywordReply.MateType = request.MateType;
            await _keywordReplyRepository.UpdateAsync(keywordReply);
            if (await Commit())
            {
                var key = GirvsEntityCacheDefaults<KeywordReply>.ByIdCacheKey.Create(keywordReply.Id.ToString());

                await _bus.RaiseEvent(new SetCacheEvent(keywordReply, key, key.CacheTime), cancellationToken);
                // 删除查询相关的缓存
                await _bus.RaiseEvent(new RemoveCacheListEvent(GirvsEntityCacheDefaults<KeywordReply>.ListCacheKey.Create()), cancellationToken);
            }
            return true;
        }

        /// <summary>
        /// 删除规则
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<bool> Handle(DeleteKeywordReplyCommand request, CancellationToken cancellationToken)
        {
            KeywordReply? keywordReply = await _keywordReplyRepository.GetByIdAsync(request.Id);
            if (keywordReply == null)
            {
                await _bus.RaiseEvent(new DomainNotification(request.Id.ToString(), "没有找到对应的规则"), cancellationToken);
                return false;
            }
            await _keywordReplyRepository.DeleteAsync(keywordReply);
            if (await Commit())
            {
                var key = GirvsEntityCacheDefaults<KeywordReply>.ByIdCacheKey.Create(keywordReply.Id.ToString());

                await _bus.RaiseEvent(new SetCacheEvent(keywordReply, key, key.CacheTime), cancellationToken);
                // 删除查询相关的缓存
                await _bus.RaiseEvent(new RemoveCacheListEvent(GirvsEntityCacheDefaults<KeywordReply>.ListCacheKey.Create()), cancellationToken);
            }
            return true;
        }
    }
}
