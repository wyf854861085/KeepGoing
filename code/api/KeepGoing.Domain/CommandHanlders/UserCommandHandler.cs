﻿using Girvs.BusinessBasis.UoW;
using Girvs.Cache.Caching;
using Girvs.Driven.Bus;
using Girvs.Driven.CacheDriven.Events;
using Girvs.Driven.Commands;
using Girvs.Driven.Notifications;
using Girvs.Extensions;
using KeepGoing.Domain.Commands.User;
using KeepGoing.Domain.Extensions;
using KeepGoing.Domain.Models;
using KeepGoing.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Domain.CommandHanlders
{
    public class UserCommandHandler :
        CommandHandler,
        IRequestHandler<CreateUserCommand, bool>,
        IRequestHandler<UpdataUserCommand, bool>,
        IRequestHandler<DeleteUserCommand, bool>,
        IRequestHandler<EditUserRoleCommand, bool>,
        IRequestHandler<UpdateUserErrorInfoCommand, bool>
    {
        private readonly IMediatorHandler _bus;
        private readonly IUserRepository _userRepository;

        private readonly IRoleRepository _roleRepository;

        public UserCommandHandler(
            IUnitOfWork<User> uow,
            IMediatorHandler bus,
            IUserRepository userRepository,
            IRoleRepository roleRepository) : base(uow, bus)
        {
            _bus = bus ?? throw new ArgumentNullException(nameof(bus));
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
            _roleRepository = roleRepository;
        }

        /// <summary>
        /// 创建用户
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<bool> Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            if (await _userRepository.ExistEntityAsync(x => x.UserAccount == request.UserAccount))
            {
                await _bus.RaiseEvent(new DomainNotification(request.UserAccount.ToString(), "登录名称已经存在"), cancellationToken);
                return false;
            }
            User user = new User
            {
                UserName = request.UserName,
                UserAccount = request.UserAccount,
                UserPassword = request.UserPassword.ToMd5(),
                CreatorId = request.CreatorId,
            };
            await _userRepository.AddAsync(user);
            if (await Commit())
            {
                var key = GirvsEntityCacheDefaults<User>.ByIdCacheKey.Create(user.Id.ToString());

                await _bus.RaiseEvent(new SetCacheEvent(user, key, key.CacheTime), cancellationToken);
                // 删除查询相关的缓存
                await _bus.RaiseEvent(new RemoveCacheListEvent(GirvsEntityCacheDefaults<User>.ListCacheKey.Create()), cancellationToken);
            }
            return true;
        }

        /// <summary>
        /// 修改用户名称
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<bool> Handle(UpdataUserCommand request, CancellationToken cancellationToken)
        {
            User user = await _userRepository.GetByIdAsync(request.Id);
            if (user == null)
            {
                await _bus.RaiseEvent(new DomainNotification(request.Id.ToString(), "修改失败，用户不存在"), cancellationToken);
                return false;
            }
            user.UserName = request.UserName;
            await _userRepository.UpdateAsync(user);
            if (await Commit())
            {
                var key = GirvsEntityCacheDefaults<User>.ByIdCacheKey.Create(user.Id.ToString());

                await _bus.RaiseEvent(new SetCacheEvent(user, key, key.CacheTime), cancellationToken);
                // 删除查询相关的缓存
                await _bus.RaiseEvent(new RemoveCacheListEvent(GirvsEntityCacheDefaults<User>.ListCacheKey.Create()), cancellationToken);
            }
            return true;
        }

        /// <summary>
        /// 禁用用户
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<bool> Handle(DeleteUserCommand request, CancellationToken cancellationToken)
        {
            User user = await _userRepository.GetByIdAsync(request.Id);
            user.UserState = user.UserState == UserState.Disable ? UserState.Enable : UserState.Disable;
            await _userRepository.UpdateAsync(user);
            if (await Commit())
            {
                var key = GirvsEntityCacheDefaults<User>.ByIdCacheKey.Create(request.Id.ToString());

                await _bus.RaiseEvent(new SetCacheEvent(user, key, key.CacheTime), cancellationToken);
                // 删除查询相关的缓存
                await _bus.RaiseEvent(new RemoveCacheListEvent(GirvsEntityCacheDefaults<User>.ListCacheKey.Create()), cancellationToken);
            }
            return true;
        }

        public async Task<bool> Handle(EditUserRoleCommand request, CancellationToken cancellationToken)
        {
            User? user = await _userRepository.GetUserByRoleAsync(request.Id);
            if (user == null)
            {
                await _bus.RaiseEvent(new DomainNotification(request.Id.ToString(), "没有找到对应用户"), cancellationToken);
                return false;
            }
            user.Roles = await _roleRepository.GetWhereAsync(x => request.RoleIds.Contains(x.Id));
            await _userRepository.UpdateAsync(user);
            await Commit();
            return true;
        }
        /// <summary>
        /// 修改用户登录错误信息
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>

        public async Task<bool> Handle(UpdateUserErrorInfoCommand request, CancellationToken cancellationToken)
        {
            User? user = await _userRepository.GetUserByRoleAsync(request.Id);
            if (user == null)
            {
                await _bus.RaiseEvent(new DomainNotification(request.Id.ToString(), "没有找到对应用户"), cancellationToken);
                return false;
            }
            user.ErrorCount = request.ErrorCount;
            user.ErrorTime = request.ErrorTime;
            await _userRepository.UpdateAsync(user);
            await Commit();
            return true;
        }
    }
}
