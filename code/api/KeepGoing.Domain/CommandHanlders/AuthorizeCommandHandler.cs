﻿using Girvs.BusinessBasis.UoW;
using Girvs.Cache.Caching;
using Girvs.Driven.Bus;
using Girvs.Driven.CacheDriven.Events;
using Girvs.Driven.Commands;
using KeepGoing.Domain.Commands.BasalPermission;
using KeepGoing.Domain.Models;
using KeepGoing.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Domain.CommandHanlders
{
    public class AuthorizeCommandHandler : CommandHandler,
       IRequestHandler<NeedAuthorizeListCommand, bool>
    {
        [NotNull] private readonly IMediatorHandler _bus;
        [NotNull] private readonly IUnitOfWork<ServicePermission> _unitOfWork;
        [NotNull] private readonly IServicePermissionRepository _servicePermissionRepository;

        public AuthorizeCommandHandler(
            [NotNull] IMediatorHandler bus,
            [NotNull] IUnitOfWork<ServicePermission> unitOfWork,
            [NotNull] IServicePermissionRepository servicePermissionRepository
        ) : base(unitOfWork, bus)
        {
            _bus = bus ?? throw new ArgumentNullException(nameof(bus));
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _servicePermissionRepository = servicePermissionRepository ??
                                           throw new ArgumentNullException(nameof(servicePermissionRepository));
        }

        public async Task<bool> Handle(NeedAuthorizeListCommand request, CancellationToken cancellationToken)
        {
            Expression<Func<ServicePermission, bool>> permissionExpression = x =>
                request.ServicePermissionCommandModels.Select(x => x.ServiceId).ToList().Contains(x.ServiceId);

            var oldsps = await _servicePermissionRepository.GetWhereAsync(permissionExpression);

            var newsps = new List<ServicePermission>();

            newsps.AddRange(request.ServicePermissionCommandModels.Select(x => new ServicePermission()
            {
                ServiceId = x.ServiceId,
                ServiceName = x.ServiceName,
                OperationPermissions = x.OperationPermissionModels,
                Permissions = x.Permissions,
                Order = x.Order,
                Tag = x.Tag,
                FuncModule = x.FuncModule,
                OtherParams = x.OtherParams
            }));

            await _servicePermissionRepository.DeleteRangeAsync(oldsps);
            await _servicePermissionRepository.AddRangeAsync(newsps);

            if (await Commit())
            {
                await _bus.RaiseEvent(
                    new RemoveCacheListEvent(GirvsEntityCacheDefaults<ServicePermission>.ListCacheKey.Create()),
                    cancellationToken);
            }

            return true;
        }
    }
}