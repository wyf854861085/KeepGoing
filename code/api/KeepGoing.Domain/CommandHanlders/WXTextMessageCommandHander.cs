﻿using Girvs.BusinessBasis.UoW;
using Girvs.Cache.Caching;
using Girvs.Driven.Bus;
using Girvs.Driven.CacheDriven.Events;
using Girvs.Driven.Commands;
using Girvs.Driven.Notifications;
using KeepGoing.Domain.Commands.User;
using KeepGoing.Domain.Commands.WXTextMessage;
using KeepGoing.Domain.Models;
using KeepGoing.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Domain.CommandHanlders
{
    public class WXTextMessageCommandHander : CommandHandler,
         IRequestHandler<CreateWXTextMessageCommand, bool>
    {
        private readonly IMediatorHandler _bus;

        private readonly IIWXTextMessageRepository _iWXTextMessageRepository;

        public WXTextMessageCommandHander(
            IUnitOfWork<WXTextMessage> uow, 
            IMediatorHandler bus, 
            IIWXTextMessageRepository iWXTextMessageRepository) : base(uow, bus)
        {
            _bus = bus ?? throw new ArgumentNullException(nameof(bus));
            _iWXTextMessageRepository = iWXTextMessageRepository ?? throw new ArgumentNullException(nameof(iWXTextMessageRepository));
        }

        public async Task<bool> Handle(CreateWXTextMessageCommand request, CancellationToken cancellationToken)
        {
            WXTextMessage wXTextMessage = new WXTextMessage
            {
                AppId = request.AppId,
                Content = request.Content,
                CreateTime = DateTime.Now,
                FromUserName = request.FromUserName,
                Id = Guid.NewGuid(),
                Idx = request.Idx,
                ReplyContent= request.ReplyContent,
                MsgDataId = request.MsgDataId,
                MsgId = request.MsgId,
                MsgType = request.MsgType,
                ToUserName = request.ToUserName,
            };
            await _iWXTextMessageRepository.AddAsync(wXTextMessage);
            if (await Commit())
            {
                //var key = GirvsEntityCacheDefaults<WXTextMessage>.ByIdCacheKey.Create(wXTextMessage.Id.ToString());

                //await _bus.RaiseEvent(new SetCacheEvent(wXTextMessage, key, key.CacheTime), cancellationToken);
                //// 删除查询相关的缓存
                //await _bus.RaiseEvent(new RemoveCacheListEvent(GirvsEntityCacheDefaults<WXTextMessage>.ListCacheKey.Create()), cancellationToken);
            }
            return true;
        }
    }
}
