﻿using Girvs.BusinessBasis.UoW;
using Girvs.Cache.Caching;
using Girvs.Driven.Bus;
using Girvs.Driven.CacheDriven.Events;
using Girvs.Driven.Commands;
using Girvs.Driven.Notifications;
using KeepGoing.Domain.Commands.Role;
using KeepGoing.Domain.Commands.User;
using KeepGoing.Domain.Models;
using KeepGoing.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Domain.CommandHanlders
{
    public class RoleCommandHandler : CommandHandler,
        IRequestHandler<CreateRoleCommand, bool>,
        IRequestHandler<UpdataRoleCommand, bool>,
        IRequestHandler<DeleteRoleCommand, bool>
    {
        private readonly IMediatorHandler _bus;
        private readonly IRoleRepository _roleRepository;

        public RoleCommandHandler(
            IUnitOfWork<Role> uow,
            IMediatorHandler bus,
            IRoleRepository roleRepository) : base(uow, bus)
        {
            _bus = bus ?? throw new ArgumentNullException(nameof(bus));
            _roleRepository = roleRepository ?? throw new ArgumentNullException(nameof(roleRepository));
        }
        /// <summary>
        /// 创建角色
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<bool> Handle(CreateRoleCommand request, CancellationToken cancellationToken)
        {
            if (await _roleRepository.ExistEntityAsync(x => x.RoleName == request.RoleName))
            {
                await _bus.RaiseEvent(new DomainNotification(request.RoleName.ToString(), "角色名称已经存在"), cancellationToken);
                return false;
            }
            Role role = new Role
            {
                RoleName = request.RoleName,
                RoleDesc = request.RoleDesc,
                Sort = request.Sort,
            };
            await _roleRepository.AddAsync(role);
            if (await Commit())
            {
                var key = GirvsEntityCacheDefaults<Role>.ByIdCacheKey.Create(role.Id.ToString());

                await _bus.RaiseEvent(new SetCacheEvent(role, key, key.CacheTime), cancellationToken);
                // 删除查询相关的缓存
                await _bus.RaiseEvent(new RemoveCacheListEvent(GirvsEntityCacheDefaults<Role>.ListCacheKey.Create()), cancellationToken);
            }
            return true;
        }
        /// <summary>
        /// 修改角色
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<bool> Handle(UpdataRoleCommand request, CancellationToken cancellationToken)
        {
            Role role = await _roleRepository.GetByIdAsync(request.Id);
            if (await _roleRepository.ExistEntityAsync(x => x.RoleName == request.RoleName && x.Id != request.Id))
            {
                await _bus.RaiseEvent(new DomainNotification(request.RoleName.ToString(), "角色名称已经存在"), cancellationToken);
                return false;
            }
            role.RoleName = request.RoleName;
            role.RoleDesc = request.RoleDesc;
            role.Sort = request.Sort;
            if (await Commit())
            {
                var key = GirvsEntityCacheDefaults<Role>.ByIdCacheKey.Create(role.Id.ToString());

                await _bus.RaiseEvent(new SetCacheEvent(role, key, key.CacheTime), cancellationToken);
                // 删除查询相关的缓存
                await _bus.RaiseEvent(new RemoveCacheListEvent(GirvsEntityCacheDefaults<Role>.ListCacheKey.Create()), cancellationToken);
            }
            return true;
        }

        /// <summary>
        /// 删除角色
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<bool> Handle(DeleteRoleCommand request, CancellationToken cancellationToken)
        {
            Role role = await _roleRepository.GetByIdAsync(request.Id);
            await _roleRepository.DeleteAsync(role);
            if (await Commit())
            {
                var key = GirvsEntityCacheDefaults<Role>.ByIdCacheKey.Create(role.Id.ToString());

                await _bus.RaiseEvent(new SetCacheEvent(role, key, key.CacheTime), cancellationToken);
                // 删除查询相关的缓存
                await _bus.RaiseEvent(new RemoveCacheListEvent(GirvsEntityCacheDefaults<Role>.ListCacheKey.Create()), cancellationToken);
            }
            return true;
        }
       
    }
}
