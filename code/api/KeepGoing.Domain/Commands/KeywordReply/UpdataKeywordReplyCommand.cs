﻿using FluentValidation;
using Girvs.Driven.Commands;
using KeepGoing.Domain.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Domain.Commands.KeywordReply
{
    public record UpdataKeywordReplyCommand(
       Guid Id,
       string RuleName,
       MateTypeEnum MateType,
       string KeywordReplyName,
       string ReplyContent
       ) : Command("修改回复规则")
    {
        public override void AddFluentValidationRule<TCommand>(AbstractValidator<TCommand> validator)
        {
            validator.RuleFor(x => Id).NotEmpty().WithMessage("回复Id不能为空");
            validator.RuleFor(x => RuleName).NotEmpty().WithMessage("规则名称不能为空");
            validator.RuleFor(x => KeywordReplyName).NotEmpty().WithMessage("关键字内容不能为空");
            validator.RuleFor(x => ReplyContent).NotEmpty().WithMessage("回复内容不能为空");
            base.AddFluentValidationRule(validator);
        }
    }
}
