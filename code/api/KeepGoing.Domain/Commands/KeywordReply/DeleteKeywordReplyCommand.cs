﻿using FluentValidation;
using Girvs.Driven.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Domain.Commands.KeywordReply
{
    /// <param name="Id"></param>
    public record DeleteKeywordReplyCommand(Guid Id) : Command("删除回复规则")
    {
        public override void AddFluentValidationRule<TCommand>(AbstractValidator<TCommand> validator)
        {
            validator.RuleFor(x => Id).NotEmpty().WithMessage("回复规则Id不能为空");
            base.AddFluentValidationRule(validator);
        }
    }
}
