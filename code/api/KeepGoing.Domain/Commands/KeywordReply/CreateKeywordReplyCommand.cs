﻿using FluentValidation;
using Girvs.Driven.Commands;
using KeepGoing.Domain.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Domain.Commands.KeywordReply
{

    public record CreateKeywordReplyCommand(
       string AppId,
       string RuleName,
       MateTypeEnum MateType,
       string KeywordReplyName,
       string ReplyContent,
       string imageFile,
       string txtFile
       ) : Command("创建回复规则")
    {
        public override void AddFluentValidationRule<TCommand>(AbstractValidator<TCommand> validator)
        {
            validator.RuleFor(x => AppId).NotEmpty().WithMessage("公众号Id不能为空");
            validator.RuleFor(x => RuleName).NotEmpty().WithMessage("规则名称不能为空");
            validator.RuleFor(x => KeywordReplyName).NotEmpty().WithMessage("关键字内容不能为空");
            validator.RuleFor(x => ReplyContent).NotEmpty().WithMessage("回复内容不能为空");
            base.AddFluentValidationRule(validator);
        }
    }
}
