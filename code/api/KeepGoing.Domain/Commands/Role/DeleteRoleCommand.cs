﻿using FluentValidation;
using Girvs.Driven.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Domain.Commands.Role
{
    /// <param name="Id"></param>
    public record DeleteRoleCommand(Guid Id) : Command("删除角色")
    {
        public override void AddFluentValidationRule<TCommand>(AbstractValidator<TCommand> validator)
        {
            validator.RuleFor(x => Id).NotEmpty().WithMessage("角色编码不能为空");
            base.AddFluentValidationRule(validator);
        }
    }
}
