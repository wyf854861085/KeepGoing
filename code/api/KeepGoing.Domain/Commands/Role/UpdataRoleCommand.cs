﻿using FluentValidation;
using Girvs.Driven.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Domain.Commands.Role
{
    public record UpdataRoleCommand(
        Guid Id,
       string RoleName,
       string RoleDesc,
       int Sort
       ) : Command("修改角色")
    {
        public override void AddFluentValidationRule<TCommand>(AbstractValidator<TCommand> validator)
        {
            validator.RuleFor(x => Id).NotEmpty().WithMessage("角色编码不能为空");
            validator.RuleFor(x => RoleName).NotEmpty().WithMessage("角色名称不能为空");
            validator.RuleFor(x => RoleDesc).NotEmpty().WithMessage("角色描述不能为空");
            base.AddFluentValidationRule(validator);
        }
    }
}
