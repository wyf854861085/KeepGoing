﻿using Girvs.Driven.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Domain.Commands.WXTextMessage
{
    public record CreateWXTextMessageCommand(string AppId, string? Content, string? ToUserName, string? FromUserName, string? MsgType, long MsgId, long MsgDataId, long Idx, string? ReplyContent) : Command("添加微信回复信息记录")
    {

    }
}
