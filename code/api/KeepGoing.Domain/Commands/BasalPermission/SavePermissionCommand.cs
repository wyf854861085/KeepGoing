﻿using Girvs.AuthorizePermission.Enumerations;
using Girvs.Driven.Commands;

namespace KeepGoing.Domain.Commands.BasalPermission;

/// <summary>
/// 保存操作权限
/// </summary>
/// <param name="AppliedID">对应用户ID或角色ID</param>
/// <param name="AppliedObjectType">对应功能模块的ID</param>
/// <param name="ValidateObjectType">权限分类</param>
/// <param name="ObjectPermissions">权限数据</param>
public record SavePermissionCommand(Guid AppliedID, PermissionAppliedObjectType AppliedObjectType,
    PermissionValidateObjectType ValidateObjectType, IList<ObjectPermission> ObjectPermissions) : Command(
    "保存操作权限");

public class ObjectPermission
{
    public ObjectPermission()
    {
        PermissionOpeation = new List<Permission>();
    }

    /// <summary>
    /// 对应功能模块的ID
    /// </summary>
    public Guid AppliedObjectID { get; set; }

    /// <summary>
    /// 权限值转换具体说明集合
    /// </summary>
    public List<Permission> PermissionOpeation { get; set; }
}