﻿using Girvs.AuthorizePermission.Enumerations;
using Girvs.AuthorizePermission;
using Girvs.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Girvs.Driven.Commands;

namespace KeepGoing.Domain.Commands.BasalPermission;
/// <summary>
/// 初始化服务需要授权列表
/// </summary>
/// <param name="ServicePermissionCommandModels">操作权限</param>
/// <param name="ServiceDataRuleCommandModels">数据权限</param>
public record NeedAuthorizeListCommand(List<ServicePermissionCommandModel> ServicePermissionCommandModels) : Command("初始化服务需要授权列表");

public class ServicePermissionCommandModel
{
    public string ServiceName { get; set; }
    public Guid ServiceId { get; set; }
    public Dictionary<string, string> Permissions { get; set; }
    public List<OperationPermissionModel> OperationPermissionModels { get; set; }
    /// <summary>
    /// 所属标签
    /// </summary>
    public string Tag { get; set; }

    /// <summary>
    /// 排序
    /// </summary>
    public int Order { get; set; }

    /// <summary>
    /// 所属的子系统模块
    /// </summary>
    public SystemModule FuncModule { get; set; }

    /// <summary>
    /// 其它相关参数
    /// </summary>
    public string[] OtherParams { get; set; }
}


public class ServiceDataRuleCommandModel
{
    /// <summary>
    /// 实体说明
    /// </summary>
    public string EntityDesc { get; set; }

    /// <summary>
    /// 服务名称
    /// </summary>
    public string EntityTypeName { get; set; }

    /// <summary>
    /// 字段名称
    /// </summary>
    public string FieldName { get; set; }

    /// <summary>
    /// 字段的说明
    /// </summary>
    public string FieldDesc { get; set; }

    /// <summary>
    /// 字段类型（预留）
    /// </summary>
    public string FieldType { get; set; }

    /// <summary>
    /// 字段赋值
    /// </summary>
    public string FieldValue { get; set; }

    /// <summary>
    /// 表达式运算符
    /// </summary>
    public ExpressionType ExpressionType { get; set; }

    /// <summary>
    /// 用户类型
    /// </summary>
    public UserType UserType { get; set; }

    /// <summary>
    /// 所属标签
    /// </summary>
    public string Tag { get; set; }

    /// <summary>
    /// 排序
    /// </summary>
    public int Order { get; set; }
}