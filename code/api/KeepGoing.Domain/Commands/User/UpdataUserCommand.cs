﻿using FluentValidation;
using Girvs.Driven.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Domain.Commands.User
{
    
    public record UpdataUserCommand(
        Guid Id,
        string UserName
        ) : Command("修改用户名称")
    {
        
    }
}
