﻿using FluentValidation;
using FluentValidation.Results;
using Girvs.Driven.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Domain.Commands.User
{
    public record CreateUserCommand(
        string UserAccount,
        string UserName,
        string UserPassword,
        string? ContactNumber,
        Guid CreatorId) : Command("创建用户")
    {
        public override void AddFluentValidationRule<TCommand>(AbstractValidator<TCommand> validator)
        {
            validator.RuleFor(x => UserAccount).NotEmpty().WithMessage("登录名称不能为空").MaximumLength(18).WithMessage("登录名称长度不能大于18");
            base.AddFluentValidationRule(validator);
        }
    }
}
