﻿using Girvs.Driven.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Domain.Commands.User
{
    public record UpdateUserErrorInfoCommand(Guid Id, int ErrorCount, DateTime? ErrorTime) : Command("修改用户登录异常信息")
    {

    }
}
