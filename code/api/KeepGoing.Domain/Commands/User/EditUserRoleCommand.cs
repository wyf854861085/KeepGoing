﻿using Girvs.Driven.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Domain.Commands.User
{
    public record EditUserRoleCommand(Guid Id, List<Guid> RoleIds) : Command("修改用户所属角色");

}
