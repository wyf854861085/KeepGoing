﻿using FluentValidation;
using Girvs.Driven.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Domain.CommandHanlders
{
    public record DeleteUserCommand(Guid Id) : Command("禁用用户")
    {

    }
}
