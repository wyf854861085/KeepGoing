﻿using Girvs.Extensions;
using Girvs.Infrastructure;
using KeepGoing.Domain.Models;
using KeepGoing.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Domain.Extensions
{
    public static class EngineContextExtensions
    {
        public static User GetCurrentUser(this IEngine engine)
        {
            var userId = engine.ClaimManager.GetUserId();
            if (userId.IsNullOrEmpty()) return null;
            var userRepository = engine.Resolve<IUserRepository>();
            return userRepository.GetByIdAsync(userId.ToHasGuid().Value).Result;
        }
    }
}
