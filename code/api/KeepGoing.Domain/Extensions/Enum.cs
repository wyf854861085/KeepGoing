﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Domain.Extensions
{
    /// <summary>
    /// 用户状态
    /// </summary>
    public enum UserState
    {
        /// <summary>
        /// 禁用
        /// </summary>

        [DescriptionAttribute("禁用")]
        Disable = 0,

        /// <summary>
        /// 启用
        /// </summary>
        [DescriptionAttribute("启用")]
        Enable = 1
    }
    /// <summary>
    /// 匹配类型
    /// </summary>

    public enum MateTypeEnum
    {
        /// <summary>
        /// 完全匹配
        /// </summary>

        [DescriptionAttribute("完全匹配")]
        PerfectMatch = 0,

        /// <summary>
        /// 部分匹配
        /// </summary>
        [DescriptionAttribute("部分匹配")]
        PartialMatching = 1

    }
}
