﻿using Girvs.AuthorizePermission.Enumerations;
using KeepGoing.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Domain.Extensions
{
    public static class PermissionHelper
    {
        public static int ConvertStringToPermissionValue(List<string> permissionStrs)
        {
            var validateObjectID = Permission.Undefined;
            foreach (var permissionStr in permissionStrs)
            {
                Permission c = (Permission)Enum.Parse(typeof(Permission), permissionStr, true);
                validateObjectID = validateObjectID | c;
            }

            return (int)validateObjectID;
        }

        public static List<Permission> ConvertStringToPermission(List<string> permissionStrs)
        {
            List<Permission> permissions = new List<Permission>();
            foreach (var permissionStr in permissionStrs)
            {
                var permission = (Permission)Enum.Parse(typeof(Permission), permissionStr, true);
                permissions.Add(permission);
            }

            return permissions;
        }

        public static List<Permission> ConvertStringToPermission(List<int> permissionStrs)
        {
            List<Permission> permissions = new List<Permission>();
            foreach (var permissionStr in permissionStrs)
            {
                var permission = (Permission)(permissionStr);
                permissions.Add(permission);
            }

            return permissions;
        }

        public static List<string> ConvertPermissionToString(BasalPermission basalPermission)
        {
            var list = new List<string>();
            foreach (Permission value in typeof(Permission).GetEnumValues())
            {
                if (basalPermission.GetBit(value))
                {
                    list.Add(value.ToString());
                }
            }

            return list;
        }

        public static List<int> ConvertPermissionToInt(BasalPermission basalPermission)
        {
            var list = new List<int>();
            foreach (Permission value in typeof(Permission).GetEnumValues())
            {
                if (basalPermission.GetBit(value))
                {
                    list.Add((int)value);
                }
            }

            return list;
        }

        /// <summary>
        /// 对权限进行合并
        /// </summary>
        /// <param name="ps"></param>
        /// <returns></returns>
        public static List<BasalPermission> MergeValidateObjectTypePermission(List<BasalPermission> ps)
        {
            var psGroup = ps.GroupBy(p => p.AppliedObjectID).ToList();
            var newPs = new List<BasalPermission>();
            foreach (var item in psGroup)
            {
                if (!item.Any()) continue;
                var allowMask = Permission.Undefined;
                var denyMask = Permission.Undefined;
                foreach (var p in item)
                {
                    allowMask |= p.AllowMask;
                    denyMask |= p.DenyMask;
                }

                var m = item.FirstOrDefault();
                m.AllowMask = allowMask;
                m.DenyMask = denyMask;
                newPs.Add(m);
            }

            return newPs;
        }
    }
}