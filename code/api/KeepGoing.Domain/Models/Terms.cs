﻿using Girvs.BusinessBasis.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Domain.Models
{
    public class Terms
    {

        [Key]
        [MaxLength(10)]
        public string Text { get; set; }

    }
}
