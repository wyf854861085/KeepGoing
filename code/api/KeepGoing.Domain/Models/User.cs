﻿using Girvs.AuthorizePermission.Enumerations;
using Girvs.BusinessBasis.Entities;
using KeepGoing.Domain.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Domain.Models
{
    public class User : AggregateRoot<Guid>, IIncludeInitField, IIncludeCreateTime, IIncludeCreatorId<Guid>
    {

        /// <summary>
        /// 登录名称
        /// </summary>
        public string UserAccount { get; set; }

        /// <summary>
        /// 用户密码
        /// </summary>
        public string UserPassword { get; set; }

        /// <summary>
        /// 用户名称
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 用户联系方式
        /// </summary>
        public string? ContactNumber { get; set; }

        /// <summary>
        /// 登录失败次数
        /// </summary>
        public int ErrorCount { get; set; } = 0;

        /// <summary>
        /// 登录限制时间
        /// </summary>
        public DateTime? ErrorTime { get; set; }

        /// <summary>
        /// 用户类型
        /// </summary>
        public UserType UserType { get; set; }

        /// <summary>
        /// 用户状态
        /// </summary>
        public UserState UserState { get; set; } = UserState.Enable;

        /// <summary>
        /// 角色集合
        /// </summary>
        public virtual List<Role>? Roles { get; set; }

        /// <summary>
        /// 是否初始化数据
        /// </summary>
        public bool IsInitData { get; set; }


        public DateTime CreateTime { get; set; }

        public Guid CreatorId { get; set; }
    }
}
