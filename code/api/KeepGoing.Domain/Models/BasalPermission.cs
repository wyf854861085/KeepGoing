﻿using Girvs.AuthorizePermission.Enumerations;
using Girvs.BusinessBasis.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Domain.Models
{
    /// <summary>
    /// BasalPermission 对象影射到表 'Permissions'.
    /// </summary>
    public class BasalPermission : PermissionBase
    {
        #region Public Properties

        /// <summary>
        /// 对应用户ID或角色ID
        /// </summary>
        public Guid AppliedID { get; set; }

        /// <summary>
        /// 对应功能模块的ID
        /// </summary>
        public Guid AppliedObjectID { get; set; }

        /// <summary>
        /// 对应授权的类型
        /// </summary>
        public PermissionAppliedObjectType AppliedObjectType { get; set; }

        /// <summary>
        /// 权限值
        /// </summary>
        public int ValidateObjectID { get; set; }

        /// <summary>
        /// 权限分类，方便扩展
        /// </summary>
        public PermissionValidateObjectType ValidateObjectType { get; set; }

        #endregion

    }
}
