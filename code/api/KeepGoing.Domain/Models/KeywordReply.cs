﻿using Girvs.BusinessBasis.Entities;
using KeepGoing.Domain.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Domain.Models
{
    [Table("KeywordReply")]
    public class KeywordReply : AggregateRoot<Guid>, IIncludeCreateTime
    {
        /// <summary>
        /// 公众号Id
        /// </summary>
        public string? AppId { get; set; }

        /// <summary>
        /// 规则名称
        /// </summary>
        public string? RuleName { get; set; }

        /// <summary>
        /// 关键字匹配类型
        /// </summary>
        public MateTypeEnum MateType { get; set; }

        /// <summary>
        /// 关键字内容
        /// </summary>
        public string? KeywordReplyName { get; set; }

        /// <summary>
        /// 回复内容
        /// </summary>
        public string? ReplyContent { get; set; }

        /// <summary>
        /// 封面路径
        /// </summary>
        public string imageFile { get; set; }

        /// <summary>
        /// 文本路径
        /// </summary>
        public string txtFile { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; } = DateTime.Now;
    }
}
