﻿using Girvs.BusinessBasis.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Domain.Models
{
    public class WXTextMessage : AggregateRoot<Guid>, IIncludeCreateTime
    {
        /// <summary>
        /// 微信Id
        /// </summary>
        public string AppId { get; set; }

        /// <summary>
        /// 文本消息内容
        /// </summary>

        public string? Content { get; set; }

        /// <summary>
        /// 开发者微信号
        /// </summary> 
        public string? ToUserName { get; set; }
        /// <summary>
        /// 发送方帐号（一个OpenID）
        /// </summary>  
        public string? FromUserName { get; set; }

        /// <summary>
        /// 消息类型，文本为text
        /// </summary> 
        public string? MsgType { get; set; }
        /// <summary>
        /// 消息id
        /// </summary> 
        public long MsgId { get; set; }
        /// <summary>
        /// 消息的数据ID（消息如果来自文章时才有）
        /// </summary> 
        public long MsgDataId { get; set; }
        /// <summary>
        /// 多图文时第几篇文章，从1开始（消息如果来自文章时才有）
        /// </summary> 
        public long Idx { get; set; }

        /// <summary>
        /// 回复内容
        /// </summary>
        public string? ReplyContent { get; set; }

        public DateTime CreateTime { get; set; }
    }
}
