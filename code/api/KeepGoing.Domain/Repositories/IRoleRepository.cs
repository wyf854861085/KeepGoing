﻿using Girvs.BusinessBasis.Repositories;
using KeepGoing.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Domain.Repositories
{
    public interface IRoleRepository : IRepository<Role>
    {
     
    }
}
