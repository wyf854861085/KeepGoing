﻿using Girvs.BusinessBasis.Repositories;
using KeepGoing.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Domain.Repositories
{
    public interface IIWXTextMessageRepository : IRepository<WXTextMessage>
    {

        /// <summary>
        /// 获取回复次数
        /// </summary>
        /// <param name="AppId"></param>
        /// <param name="Content"></param>
        /// <returns></returns>
        public Task<int> GetCountByWxId(string? AppId, string? Content);


        /// <summary>
        /// 获取需要发表的文字内容
        /// </summary>
        /// <param name="AppId"></param>
        /// <param name="FromUserName"></param>
        /// <returns></returns>
        Task<List<WXTextMessage>> GetByWxIdFromUserName(string AppId, string FromUserName);


        /// <summary>
        /// 获取随机的一个词语
        /// </summary>
        /// <returns></returns>
        public Task<string?> GetRandText();


        Task<bool> AddWXTextMessage(string AppId, string Content, string ReplyContent, string FromUserName);
    }
}
