﻿using Girvs.BusinessBasis.Repositories;
using KeepGoing.Domain.Models;

namespace KeepGoing.Domain.Repositories
{
    public interface IKeywordReplyRepository : IRepository<KeywordReply>
    {
        /// <summary>
        /// 获取最新上传的图片
        /// </summary>
        /// <param name="AppId"></param>
        /// <returns></returns>
        public Task<string?> GetImageFileByAppId(string AppId);


        /// <summary>
        /// 判断关键词是否存在
        /// </summary>
        /// <param name="AppId"></param>
        /// <param name="KeywordReplyName"></param>
        /// <returns></returns>
        public Task<bool> GetKeywordReplyNameAny(string AppId, string? KeywordReplyName);
    }
}
