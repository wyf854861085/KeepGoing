﻿using Girvs.BusinessBasis.Repositories;
using KeepGoing.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Domain.Repositories
{
    public interface IPermissionRepository : IRepository<BasalPermission>
    {
        /// <summary>
        /// 获取用户所授权限
        /// </summary>
        /// <param name="userId">用户Id</param>
        /// <returns>权限列表</returns>
        Task<List<BasalPermission>> GetUserPermissionLimit(Guid userId);
    }
}
