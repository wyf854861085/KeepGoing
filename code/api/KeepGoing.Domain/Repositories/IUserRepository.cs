﻿using Girvs.BusinessBasis.Repositories;
using KeepGoing.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Domain.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
        /// <summary>
        /// 通过用户名获取用户信息
        /// </summary>
        /// <param name="loginName"></param>
        /// <returns></returns>
        Task<User?> GetUserByLoginNameAsync(string loginName);

        /// <summary>
        /// 通过用户Id获取角色信息
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<User?> GetUserByRoleAsync(Guid userId);
    }
}
