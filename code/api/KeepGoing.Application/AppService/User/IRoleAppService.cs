﻿using Girvs.BusinessBasis;
using Girvs.DynamicWebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Application.AppService.User
{
    public interface IRoleAppService : IAppWebApiService, IManager
    {
    }
}