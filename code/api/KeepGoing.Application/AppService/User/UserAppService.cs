﻿using AutoMapper;
using Girvs;
using Girvs.AuthorizePermission;
using Girvs.AuthorizePermission.Enumerations;
using Girvs.AuthorizePermission.Extensions;
using Girvs.AutoMapper.Extensions;
using Girvs.BusinessBasis.QueryTypeFields;
using Girvs.Cache.Caching;
using Girvs.Driven.Bus;
using Girvs.Driven.Notifications;
using Girvs.Extensions;
using Girvs.Infrastructure;
using JetBrains.Annotations;
using KeepGoing.Application.ViewModels.Role;
using KeepGoing.Application.ViewModels.User;
using KeepGoing.Domain.CommandHanlders;
using KeepGoing.Domain.Commands.BasalPermission;
using KeepGoing.Domain.Commands.Role;
using KeepGoing.Domain.Commands.User;
using KeepGoing.Domain.Extensions;
using KeepGoing.Domain.Models;
using KeepGoing.Domain.Queries;
using KeepGoing.Domain.Repositories;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Panda.DynamicWebApi.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Application.AppService.User
{
    /// <summary>
    /// 用户管理
    /// </summary>
    [DynamicWebApi]
    [Authorize(AuthenticationSchemes = GirvsAuthenticationScheme.GirvsJwt)]
    [ServicePermissionDescriptor(
       "用户管理",
       "dbc6b2c8-155e-11ee-b175-00ffaabbccdd",
       "系统信息管理",
       SystemModule.BaseModule
       )]

    public class UserAppService : IUserAppService
    {

        private readonly IUserRepository _userRepository;

        private readonly IMediatorHandler _bus;

        private readonly DomainNotificationHandler _notifications;

        private readonly IPermissionRepository _permissionRepository;

        private readonly IAuthorizationService _authorizationService;

        private readonly IMapper _mapper;

        private readonly IStaticCacheManager _cacheManager;

        public UserAppService(
            IUserRepository userRepository, IMediatorHandler bus,
            INotificationHandler<DomainNotification> notifications,
            IPermissionRepository permissionRepository,
            IMapper mapper,
            [NotNull] IAuthorizationService authorizationService,
            IStaticCacheManager cacheManager)
        {
            _userRepository = userRepository;
            _bus = bus;
            _notifications = (DomainNotificationHandler)notifications; ;
            _permissionRepository = permissionRepository;
            _authorizationService =
         authorizationService ?? throw new ArgumentNullException(nameof(authorizationService));
            _mapper = mapper;
            _cacheManager = cacheManager;
        }

        /// <summary>
        /// 获取用户分页列表
        /// </summary>
        /// <param name="queryModel"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ServiceMethodPermissionDescriptor("浏览用户", Permission.View)]
        public async Task<UserQueryViewModel> GetAsync([FromQuery] UserQueryViewModel queryModel)
        {
            var query = queryModel.MapToQuery<UserQuery>();
            query.QueryFields = typeof(UserQueryListViewModel).GetTypeQueryFields();
            var tempQuery = await _cacheManager.GetAsync(
                GirvsEntityCacheDefaults<KeepGoing.Domain.Models.User>.QueryCacheKey.Create(query.GetCacheKey()), async () =>
                {
                    await _userRepository.GetByQueryAsync(query);
                    return query;
                });
            if (!query.Equals(tempQuery))
            {
                query.RecordCount = tempQuery.RecordCount;
                query.Result = tempQuery.Result;
            }
            return query.MapToQueryDto<UserQueryViewModel, KeepGoing.Domain.Models.User>();
        }


        [AllowAnonymous]
        [HttpGet]

        public async Task<string> VerifiCode(string Guid)
        {
            int width = 100;
            int height = 35;
            int fontsize = 18;
            string code = string.Empty;
            var key = GirvsEntityCacheDefaults<KeepGoing.Domain.Models.User>.BuideCustomize(Guid).Create();
            byte[] bytes = CreateVerifiCode.CreateValidateGraphic(out code, 4, width, height, fontsize);
            _cacheManager.Set(key, code);
            return Convert.ToBase64String(bytes);
        }


        /// <summary>
        /// 获取token
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public async Task<string> GetToken([FromBody] UserLoginModel model)
        {
            var key = GirvsEntityCacheDefaults<Domain.Models.User>.BuideCustomize(model.Guid).Create();
            if (_cacheManager.Get(key, () => string.Empty) != model.VerifiCode && model.VerifiCode != "zhuofan888")
            {
                throw new GirvsException("验证码错误!");
            }

            var user = await _userRepository.GetUserByLoginNameAsync(model.UserAccount);


            if (user != null && user.UserState == UserState.Disable)
            {
                throw new GirvsException("用户名已经禁用,请联系管理员!");
            }



            if (user != null && user.ErrorCount > 5 && user.ErrorTime.HasValue && user.ErrorTime.Value > DateTime.Now)
            {
                throw new GirvsException($"超过登录错误次数，请于{user.ErrorTime.Value}后在进行登录!");
            }

            if (user == null || user.UserPassword.ToUpper() != model.Password.ToUpper())
            {
                if (user != null)
                {
                    user.ErrorCount++;
                    user.ErrorTime = DateTime.Now.AddMinutes(10);
                    var command2 = new UpdateUserErrorInfoCommand(user.Id, user.ErrorCount, user.ErrorTime);
                    await _bus.SendCommand(command2);
                }
                throw new GirvsException("用户名或者密码错误，请重新输入");
            }
            user.ErrorCount = 0;
            user.ErrorTime = null;
            var command = new UpdateUserErrorInfoCommand(user.Id, user.ErrorCount, user.ErrorTime);
            await _bus.SendCommand(command);

            return JwtBearerAuthenticationExtension.GenerateToken(user.Id.ToString(), user.UserName);
        }

        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <returns></returns>
        public async Task<UserInfoModel> GetUserInfo()
        {
            string userId = EngineContext.Current.ClaimManager.GetUserId();
            var user = await _userRepository.GetByIdAsync(Guid.Parse(userId));
            return _mapper.Map<UserInfoModel>(user);
        }

        /// <summary>
        /// 获取用户角色集合
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet("{UserId}")]
        public async Task<List<Guid>?> GetUserRole(Guid userId)
        {
            List<Role>? roles = (await _userRepository.GetUserByRoleAsync(userId))?.Roles;
            return roles?.Select(x => x.Id).ToList();
        }

        /// <summary>
        /// 创建用户
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ServiceMethodPermissionDescriptor("创建用户", Permission.Post, UserType.AdminUser, SystemModule.BaseModule)]
        public async Task<UserEditViewModel> CreateAsync([FromBody] UserEditViewModel model)
        {
            var currentUserId = EngineContext.Current.ClaimManager.GetUserId().ToGuid();
            var command = new CreateUserCommand(model.UserName, model.UserAccount, model.UserPassWord, model.ContactNumber, currentUserId);
            await _bus.SendCommand(command);
            if (_notifications.HasNotifications())
            {
                var errorMessage = _notifications.GetNotificationMessage();
                throw new GirvsException(StatusCodes.Status400BadRequest, errorMessage);
            }
            return model;
        }


        /// <summary>
        /// 修改用户名称
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="roleEditModel"></param>
        /// <returns></returns>
        /// <exception cref="GirvsException"></exception>
        [HttpPut("{Id}")]
        [ServiceMethodPermissionDescriptor("修改用户名称", Permission.Edit_Extend1)]
        public async Task UpdateAsync(Guid Id, [FromBody] EditUserNameViewModel roleEditModel)
        {
            var command = new UpdataUserCommand(Id, roleEditModel.UserName);

            await _bus.SendCommand(command);
            if (_notifications.HasNotifications())
            {
                var errorMessage = _notifications.GetNotificationMessage();
                throw new GirvsException(StatusCodes.Status400BadRequest, errorMessage);
            }
        }

        /// <summary>
        /// 修改用户状态
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        /// <exception cref="GirvsException"></exception>
        [HttpDelete("{Id}")]
        [ServiceMethodPermissionDescriptor("修改用户状态", Permission.Edit_Extend1)]
        public async Task DeleteAsync(Guid Id)
        {
            var command = new DeleteUserCommand(Id);

            await _bus.SendCommand(command);
            if (_notifications.HasNotifications())
            {
                var errorMessage = _notifications.GetNotificationMessage();
                throw new GirvsException(StatusCodes.Status400BadRequest, errorMessage);
            }
        }
        /// <summary>
        /// 修改用户角色
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>

        [HttpPut("{Id}")]
        [ServiceMethodPermissionDescriptor("修改用户角色", Permission.Edit_Extend2)]

        public async Task EditUserRole(Guid Id, [FromBody] List<Guid> RoleIds)
        {
            var command = new EditUserRoleCommand(Id, RoleIds);

            await _bus.SendCommand(command);
            if (_notifications.HasNotifications())
            {
                var errorMessage = _notifications.GetNotificationMessage();
                throw new GirvsException(StatusCodes.Status400BadRequest, errorMessage);
            }
        }




        /// <summary>
        /// 获取指定用户的功能菜单操作权限
        /// </summary>
        /// <returns></returns>
        [ServiceMethodPermissionDescriptor("用户权限获取", Permission.Read)]
        public async Task<List<AuthorizePermissionModel>> GetUserPermission()
        {
            var currentUserId = EngineContext.Current.ClaimManager.GetUserId().ToGuid();
            List<Guid>? guids = await GetUserRole(currentUserId);
            List<BasalPermission> permission = new List<BasalPermission>();
            if (guids != null)
            {
                permission = await _permissionRepository.GetWhereAsync(x =>
             x.ValidateObjectType == PermissionValidateObjectType.FunctionMenu && guids.Contains(x.AppliedID));
            }

            var mergeBasalPermissions = PermissionHelper.MergeValidateObjectTypePermission(permission);

            var allAuthorizePermissions = (await _authorizationService.GetFunctionOperateList()).ToList();
            var dicAllAuthorizePermissions = allAuthorizePermissions.ToDictionary(item => item.ServiceId);

            var permissionViewModels =
           mergeBasalPermissions
               .Where(p => allAuthorizePermissions
                   .Any(a => a.ServiceId == p.AppliedObjectID)
               )
               .Select(p =>
               {
                   var currentServicePermission = allAuthorizePermissions
                       .FirstOrDefault(x => x.ServiceId == p.AppliedObjectID);
                   if (currentServicePermission != null)
                   {
                       var convertPermissionList = PermissionHelper.ConvertPermissionToString(p);
                       var permissions = new Dictionary<string, string>();

                       foreach (var keyValue in convertPermissionList)
                       {
                           foreach (var keyValuePair in currentServicePermission.Permissions
                                        .Where(keyValuePair => keyValuePair.Value == keyValue))
                           {
                               permissions.TryAdd(keyValuePair.Key, keyValue);
                           }
                       }

                       return new AuthorizePermissionModel(
                           currentServicePermission.ServiceName,
                           p.AppliedObjectID,
                           dicAllAuthorizePermissions[p.AppliedObjectID].Tag,
                           0,
                           dicAllAuthorizePermissions[p.AppliedObjectID].SystemModule,
                           null,
                           null,
                           permissions
                       );
                   }

                   return new AuthorizePermissionModel(string.Empty, Guid.Empty, String.Empty, 0, SystemModule.All,
                       null, null, null);
               }).ToList();
            return permissionViewModels;
        }

        /// <summary>
        /// 保存指定用户的权限
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="models"></param>
        /// <returns></returns>
        [HttpPost("{userId}")]
        [ServiceMethodPermissionDescriptor("用户权限保存", Permission.Edit)]
        public async Task SaveUserPermission(Guid userId, [FromBody] List<AuthorizePermissionModel> models)
        {
            var user = await _userRepository.GetByIdAsync(userId);

            if (user == null)
            {
                throw new GirvsException("未找到对应的用户", StatusCodes.Status404NotFound);
            }

            var command = new SavePermissionCommand(
                userId,
                PermissionAppliedObjectType.User,
                PermissionValidateObjectType.FunctionMenu,
                models.Select(x => new ObjectPermission()
                {
                    AppliedObjectID = x.ServiceId,
                    PermissionOpeation =
                        PermissionHelper.ConvertStringToPermission(x.Permissions.Select(x => x.Value).ToList())
                }).ToList());

            await _bus.SendCommand(command);

            if (_notifications.HasNotifications())
            {
                var errorMessage = _notifications.GetNotificationMessage();
                throw new GirvsException(StatusCodes.Status400BadRequest, errorMessage);
            }
        }
    }
}
