﻿using Girvs.BusinessBasis;
using Girvs.DynamicWebApi;
using KeepGoing.Application.ViewModels.User;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Application.AppService.User
{
    internal interface IUserAppService : IAppWebApiService, IManager
    {
        /// <summary>
        /// 用户登录获取token
        /// </summary>
        /// <returns></returns>
        Task<string> GetToken([FromForm] UserLoginModel model);
    }
}
