﻿using AutoMapper;
using Girvs.AuthorizePermission.Enumerations;
using Girvs.AuthorizePermission;
using Girvs.Cache.Caching;
using Girvs.Driven.Bus;
using Girvs.Driven.Notifications;
using Girvs.Infrastructure;
using Girvs;
using KeepGoing.Domain.Commands.BasalPermission;
using KeepGoing.Domain.Extensions;
using KeepGoing.Domain.Models;
using KeepGoing.Domain.Repositories;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Panda.DynamicWebApi.Attributes;
using System.Net;
using Microsoft.AspNetCore.Hosting;
using KeepGoing.Domain.Queries;
using KeepGoing.Application.ViewModels.Role;
using Girvs.BusinessBasis.QueryTypeFields;
using Girvs.AutoMapper.Extensions;
using KeepGoing.Domain.Commands.Role;
using System.Linq;
using System.Collections.Generic;

namespace KeepGoing.Application.AppService.User
{
    /// <summary>
    /// 角色管理接口
    /// </summary>
    [DynamicWebApi]
    [Authorize(AuthenticationSchemes = GirvsAuthenticationScheme.GirvsJwt)]
    [ApiController]
    [Route("api/Role")]
    [ServicePermissionDescriptor("角色管理", "18CC8E5C-E480-0336-598A-1E52D7267B12", "系统信息管理", SystemModule.BaseModule, 3)]
    public class RoleAppService : IRoleAppService
    {
        private readonly IStaticCacheManager _cacheManager;
        private readonly IMediatorHandler _bus;
        private readonly DomainNotificationHandler _notifications;
        private readonly IRoleRepository _roleRepository;
        private readonly IMapper _mapper;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IPermissionRepository _permissionRepository;


        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="cacheManager"></param>
        /// <param name="bus"></param>
        /// <param name="notifications"></param>
        /// <param name="roleRepository"></param>
        /// <param name="mapper"></param>
        /// <param name="hostingEnvironment"></param>
        /// <param name="moduleRepository"></param>
        /// <param name="menuRepository"></param>
        /// <param name="roleJurisdictionRepository"></param>
        /// <param name="permissionRepository"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public RoleAppService(
            IStaticCacheManager cacheManager
            , IMediatorHandler bus
            , INotificationHandler<DomainNotification> notifications
            , IRoleRepository roleRepository
            , IMapper mapper
            , IHostingEnvironment hostingEnvironment
            , IPermissionRepository permissionRepository)
        {
            _cacheManager = cacheManager ?? throw new ArgumentNullException(nameof(cacheManager));
            _bus = bus ?? throw new ArgumentNullException(nameof(bus));
            _notifications = (DomainNotificationHandler)notifications;
            _roleRepository = roleRepository ?? throw new ArgumentNullException(nameof(roleRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _hostingEnvironment = hostingEnvironment;
            _permissionRepository = permissionRepository;
        }

        /// <summary>
        /// 获取角色分页列表
        /// </summary>
        /// <param name="queryModel"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ServiceMethodPermissionDescriptor("浏览角色", Permission.View)]
        public async Task<RoleQueryViewModel> GetAsync([FromQuery] RoleQueryViewModel queryModel)
        {
            var query = queryModel.MapToQuery<RoleQuery>();
            query.QueryFields = typeof(RoleQueryListViewModel).GetTypeQueryFields();
            var tempQuery = await _cacheManager.GetAsync(
                GirvsEntityCacheDefaults<Role>.QueryCacheKey.Create(query.GetCacheKey()), async () =>
                {
                    await _roleRepository.GetByQueryAsync(query);
                    return query;
                });
            if (!query.Equals(tempQuery))
            {
                query.RecordCount = tempQuery.RecordCount;
                query.Result = tempQuery.Result;
            }
            return query.MapToQueryDto<RoleQueryViewModel, Role>();
        }

        /// <summary>
        /// 获取所有角色集合
        /// </summary>
        /// <returns></returns>
        [HttpGet("RoleAll")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<List<RoleAllListViewModel>> GetRoleAllAsync()
        {
            List<Role> roles = await _roleRepository.GetAllAsync();
            return _mapper.Map<List<RoleAllListViewModel>>(roles);
        }


        /// <summary>
        /// 获取指定角色的功能菜单操作权限
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [HttpGet("{roleId}")]
        public async Task<List<RoleAuthorizePermissionModel>> GetRolePermission(Guid roleId)
        {
            var roleBasalPermissions = await _permissionRepository.GetWhereAsync(x =>
                x.AppliedObjectType == PermissionAppliedObjectType.Role &&
                x.ValidateObjectType == PermissionValidateObjectType.FunctionMenu && x.AppliedID == roleId);
            var mergeBasalPermissions = PermissionHelper.MergeValidateObjectTypePermission(roleBasalPermissions);

            return mergeBasalPermissions.Select(p => new RoleAuthorizePermissionModel(
                p.AppliedObjectID,
               PermissionHelper.ConvertPermissionToInt(p))).ToList();
        }

        /// <summary>
        /// 创建角色
        /// </summary>
        /// <param name="roleEditModel"></param>
        /// <returns></returns>
        /// <exception cref="GirvsException"></exception>
        [HttpPost]
        [ServiceMethodPermissionDescriptor("新增角色", Permission.Edit)]
        public async Task CreateRole([FromBody] RoleCreateModel roleEditModel)
        {
            var command = new CreateRoleCommand(
                roleEditModel.RoleName,
                roleEditModel.RoleDesc,
                roleEditModel.Sort);

            await _bus.SendCommand(command);

            if (_notifications.HasNotifications())
            {
                var errorMessage = _notifications.GetNotificationMessage();
                throw new GirvsException(StatusCodes.Status400BadRequest, errorMessage);
            }
        }

        /// <summary>
        /// 修改角色
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="roleEditModel"></param>
        /// <returns></returns>
        /// <exception cref="GirvsException"></exception>
        [HttpPut("{Id}")]
        [ServiceMethodPermissionDescriptor("修改角色", Permission.Edit_Extend1)]
        public async Task UpdataRole(Guid Id, [FromBody] RoleUpdataModel roleEditModel)
        {
            var command = new UpdataRoleCommand(
                Id,
                roleEditModel.RoleName,
                roleEditModel.RoleDesc,
                roleEditModel.Sort);

            await _bus.SendCommand(command);
            if (_notifications.HasNotifications())
            {
                var errorMessage = _notifications.GetNotificationMessage();
                throw new GirvsException(StatusCodes.Status400BadRequest, errorMessage);
            }
        }

        /// <summary>
        /// 删除角色
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        /// <exception cref="GirvsException"></exception>
        [HttpDelete("{Id}")]
        [ServiceMethodPermissionDescriptor("删除角色", Permission.Delete)]
        public async Task DeleteRole(Guid Id)
        {
            var command = new DeleteRoleCommand(Id);

            await _bus.SendCommand(command);
            if (_notifications.HasNotifications())
            {
                var errorMessage = _notifications.GetNotificationMessage();
                throw new GirvsException(StatusCodes.Status400BadRequest, errorMessage);
            }
        }


        /// <summary>
        /// 保存指定角色的权限
        /// </summary>
        /// <param name="models"></param>
        /// <returns></returns>
        [HttpPost("{roleId}")]
        [ServiceMethodPermissionDescriptor("修改角色权限", Permission.Edit_Extend2)]
        public async Task SaveRolePermission(Guid roleId, [FromBody] List<RoleAuthorizePermissionModel> models)
        {
            var role = await _roleRepository.GetByIdAsync(roleId);

            if (role == null)
            {
                throw new GirvsException("未找到对应的角色", StatusCodes.Status404NotFound);
            }

            var command = new SavePermissionCommand(
                roleId,
                PermissionAppliedObjectType.Role,
                PermissionValidateObjectType.FunctionMenu,
                models.Select(x => new ObjectPermission()
                {
                    AppliedObjectID = x.ServiceId,
                    PermissionOpeation =
                        PermissionHelper.ConvertStringToPermission(x.checkedData)
                }).ToList());

            await _bus.SendCommand(command);

            if (_notifications.HasNotifications())
            {
                var errorMessage = _notifications.GetNotificationMessage();
                throw new GirvsException(StatusCodes.Status400BadRequest, errorMessage);
            }

        }
    }
}

