﻿using Girvs.AuthorizePermission;
using Girvs.Cache.Caching;
using Girvs.Infrastructure;
using Girvs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Girvs.AuthorizePermission.Extensions;
using System.Diagnostics.CodeAnalysis;
using KeepGoing.Domain.Models;
using KeepGoing.Domain.Repositories;
using Microsoft.AspNetCore.Authorization;
using Panda.DynamicWebApi.Attributes;
using Girvs.AuthorizePermission.Services;
using KeepGoing.Domain.Commands.BasalPermission;
using System.Threading;
using MediatR;
using Girvs.Driven.Bus;
using Girvs.Driven.Notifications;

namespace KeepGoing.Application.AppService.User
{
    [DynamicWebApi]
    [Authorize(AuthenticationSchemes = GirvsAuthenticationScheme.GirvsJwt)]
    public class AuthorizationService : IAuthorizationService
    {
        private readonly IStaticCacheManager _cacheManager;
        private readonly IServicePermissionRepository _servicePermissionRepository;

        private readonly ILocker _locker;
        private readonly IMediatorHandler _bus;
        private DomainNotificationHandler _notifications;

        public AuthorizationService(
            [NotNull] IStaticCacheManager cacheManager,
            [NotNull] IServicePermissionRepository servicePermissionRepository,
            ILocker locker,
            [NotNull] IMediatorHandler bus,
            INotificationHandler<DomainNotification> notifications)
        {
            _cacheManager = cacheManager ?? throw new ArgumentNullException(nameof(cacheManager));
            _servicePermissionRepository = servicePermissionRepository ?? throw new ArgumentNullException(nameof(servicePermissionRepository));
            _locker = locker ?? throw new ArgumentNullException(nameof(locker));
            _bus = bus ?? throw new ArgumentNullException(nameof(bus));
            _notifications = notifications as DomainNotificationHandler ??
                             throw new ArgumentNullException(nameof(notifications));
        }

        /// <summary>
        /// 获取需要授权的功能列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IList<AuthorizePermissionModel>> GetFunctionOperateList()
        {
            var currentUser = EngineContext.Current.ClaimManager.GetUserId();

            if (currentUser == null)
            {
                throw new GirvsException(StatusCodes.Status401Unauthorized, "未授权");
            }

            var currentUserType = EngineContext.Current.ClaimManager.GetUserType();

            var availableAuthorizePermissionList = await _cacheManager.GetAsync(
                GirvsEntityCacheDefaults<ServicePermission>.AllCacheKey.Create(),
                async () =>
                    await _servicePermissionRepository.GetAllAsync());

            var currentAvailableAuthorizePermissionList = availableAuthorizePermissionList.Where(x =>
                x.OperationPermissions.Any(s => (s.UserType & currentUserType) == currentUserType)).ToList();

            var resultPermissions = new List<AuthorizePermissionModel>();
            foreach (var permissionModel in currentAvailableAuthorizePermissionList)
            {
                var permissions = new Dictionary<string, string>();
                var operationPermissions =
                    permissionModel.OperationPermissions.Where(x => (x.UserType & currentUserType) == currentUserType);
                foreach (var operationPermission in operationPermissions)
                {
                    if (!permissions.ContainsKey(operationPermission.OperationName))
                    {
                        permissions.Add(operationPermission.OperationName, operationPermission.Permission.ToString());
                    }
                }

                resultPermissions.Add(new AuthorizePermissionModel(
                    permissionModel.ServiceName,
                    permissionModel.ServiceId,
                    permissionModel.Tag,
                    permissionModel.Order,
                    permissionModel.FuncModule,
                    null,
                    null,
                    permissions
                ));
            }

            var currentSystemModules = EngineContext.Current.ClaimManager.IdentityClaim.SystemModule;

            return resultPermissions
                .Where(x => (x.SystemModule & currentSystemModules) > 0)
                .OrderBy(x => x.Tag)
                .ThenBy(x => x.Order)
                .ToList();
        }


        /// <summary>
        /// 初始化本模块的权限值
        /// </summary>
        [HttpGet]
        public async Task InitAuthorization()
        {
            IGirvsAuthorizePermissionService permissionService = new GirvsAuthorizePermissionService();
            var authorizePermissionList = permissionService?.GetAuthorizePermissionList().Result;
            var servicePermissionCommandModels = new List<ServicePermissionCommandModel>();
            foreach (var permissionAuthoriz in authorizePermissionList)
            {
                servicePermissionCommandModels.Add(new ServicePermissionCommandModel()
                {
                    ServiceName = permissionAuthoriz.ServiceName,
                    ServiceId = permissionAuthoriz.ServiceId,
                    Permissions = permissionAuthoriz.Permissions,
                    OperationPermissionModels = permissionAuthoriz.OperationPermissionModels,
                    Order = permissionAuthoriz.Order,
                    Tag = permissionAuthoriz.Tag,
                    FuncModule = permissionAuthoriz.SystemModule,
                    OtherParams = permissionAuthoriz.OtherParams
                });
            }
            var command = new NeedAuthorizeListCommand(servicePermissionCommandModels);

            await _bus.SendCommand(command);
            if (_notifications.HasNotifications())
            {
                var errorMessage = _notifications.GetNotificationMessage();
                throw new GirvsException(StatusCodes.Status400BadRequest, errorMessage);
            }
        }
    }
}
