﻿using Girvs.BusinessBasis;
using Girvs.DynamicWebApi;

namespace KeepGoing.Application.AppService.LocalFile
{
    public interface IFileService : IAppWebApiService, IManager
    {
        string Post();

        /// <summary>
        /// 获取文件路口
        /// </summary>
        /// <param name="fileId"></param>
        /// <returns></returns>
        string GetFile(string fileId);

        /// <summary>
        /// 获取访问地址
        /// </summary>
        /// <param name="fileId"></param>
        /// <returns></returns>
        string GetUrl(string fileId);
    }
}
