﻿using Girvs.AuthorizePermission;
using Girvs.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Panda.DynamicWebApi.Attributes;


namespace KeepGoing.Application.AppService.LocalFile
{
    [DynamicWebApi]
    [Authorize(AuthenticationSchemes = GirvsAuthenticationScheme.GirvsJwt)]
    [ApiController]
    [Route("api/File")]
    public class FileService : IFileService
    {
        private readonly IHostingEnvironment _hostingEnvironment;

        public readonly IConfiguration _Configs;

        public FileService(IHostingEnvironment hostingEnvironment, IConfiguration configs)
        {
            _hostingEnvironment = hostingEnvironment;
            _Configs = configs;
        }

        /// <summary>
        /// 上传文件
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public string Post()
        {
            var formFile = EngineContext.Current.HttpContext.Request.Form.Files.FirstOrDefault();
            if (formFile != null)
            {
                string contentRootPath = _hostingEnvironment.ContentRootPath;
                string fileExt = Path.GetExtension(formFile.FileName);
                string newFileName = System.Guid.NewGuid().ToString() + fileExt;
                var filePath = contentRootPath + "/fileUpload/";
                if (!Directory.Exists(filePath))
                {
                    Directory.CreateDirectory(filePath);
                }
                using (var stream = new FileStream(filePath + newFileName, FileMode.Create))
                {
                    formFile.CopyTo(stream);
                    stream.Flush();//清空文件流
                }
                return newFileName;
            }
            else
            {
                return "请选择上传文件!";
            }
        }


        /// <summary>
        /// 获取文件路口
        /// </summary>
        /// <param name="fileId"></param>
        /// <returns></returns>
        [HttpGet]
        public string GetFile(string fileId)
        {
            string contentRootPath = _hostingEnvironment.ContentRootPath;
            return Path.Combine(contentRootPath, "fileUpload", fileId);
        }

        /// <summary>
        /// 获取文件路口
        /// </summary>
        /// <param name="fileId"></param>
        /// <returns></returns>
        [HttpGet("GetUrl")]
        public string GetUrl(string fileId)
        {
            return _Configs["FileApiHost"] + "fileUpload\\" + fileId;
        }

    }
}
