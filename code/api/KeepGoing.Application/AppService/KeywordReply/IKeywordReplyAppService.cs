﻿using Girvs.BusinessBasis;
using Girvs.DynamicWebApi;
using KeepGoing.Application.ViewModels.KeywordReply;
using KeepGoing.Application.ViewModels.User;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Application.AppService.KeywordReply
{
    public interface IKeywordReplyAppService : IAppWebApiService, IManager
    {

        /// <summary>
        /// 获取匹配内容
        /// </summary>
        /// <param name="appId"></param>
        /// <param name="keywordReplyName"></param>
        /// <returns></returns>
        string? GetReplyContent(string appId, string keywordReplyName);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<string> AppCreateAsync([FromBody] KeywordReplyCreateModel model);
    }
}
