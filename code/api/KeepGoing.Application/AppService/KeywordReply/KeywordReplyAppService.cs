﻿using AutoMapper;
using Girvs;
using Girvs.AuthorizePermission;
using Girvs.AuthorizePermission.Enumerations;
using Girvs.AutoMapper.Extensions;
using Girvs.BusinessBasis.QueryTypeFields;
using Girvs.Cache.Caching;
using Girvs.Driven.Bus;
using Girvs.Driven.Notifications;
using Girvs.Infrastructure;
using KeepGoing.Application.AppService.LocalFile;
using KeepGoing.Application.ViewModels.KeywordReply;
using KeepGoing.Domain.Commands.KeywordReply;
using KeepGoing.Domain.Extensions;
using KeepGoing.Domain.Queries;
using KeepGoing.Domain.Repositories;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Panda.DynamicWebApi.Attributes;
using Senparc.Weixin;
using Senparc.Weixin.MP;
using Senparc.Weixin.MP.AdvancedAPIs.Draft.DraftJson;
using System.Net;
using System.Text.Json;

namespace KeepGoing.Application.AppService.KeywordReply
{
    /// <summary>
    /// 规则管理
    /// </summary>
    [DynamicWebApi]
    [Authorize(AuthenticationSchemes = GirvsAuthenticationScheme.GirvsJwt)]
    [ServicePermissionDescriptor(
       "规则管理",
       "0cbca915-fa6c-4cb5-a0bc-2d1eaa9a879e",
       "系统信息管理",
       SystemModule.BaseModule
       )]

    public class KeywordReplyAppService : IKeywordReplyAppService
    {

        private readonly IKeywordReplyRepository _keywordReplyRepository;

        private readonly IMediatorHandler _bus;

        private readonly DomainNotificationHandler _notifications;

        private readonly IMapper _mapper;


        private readonly IFileService _fileService;

        private readonly IStaticCacheManager _cacheManager;

        private readonly IIWXTextMessageRepository _iWXTextMessageRepository;

        public KeywordReplyAppService(
            IKeywordReplyRepository keywordReplyRepository,
            IMediatorHandler bus,
            INotificationHandler<DomainNotification> notifications,
            IMapper mapper,
            IStaticCacheManager cacheManager,
            IFileService fileService,
            IIWXTextMessageRepository iWXTextMessageRepository)
        {
            _keywordReplyRepository = keywordReplyRepository;
            _bus = bus;
            _notifications = (DomainNotificationHandler)notifications; ;
            _mapper = mapper;
            _cacheManager = cacheManager;
            _fileService = fileService;
            _iWXTextMessageRepository = iWXTextMessageRepository;
        }

        /// <summary>
        /// 获取规则分页列表
        /// </summary>
        /// <param name="queryModel"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ServiceMethodPermissionDescriptor("浏览规则", Permission.View)]
        public async Task<KeywordReplyQueryViewModel> GetAsync([FromQuery] KeywordReplyQueryViewModel queryModel)
        {
            var query = queryModel.MapToQuery<KeywordReplyQuery>();
            query.QueryFields = typeof(KeywordReplyQueryListViewModel).GetTypeQueryFields();
            var tempQuery = await _cacheManager.GetAsync(
                GirvsEntityCacheDefaults<KeepGoing.Domain.Models.KeywordReply>.QueryCacheKey.Create(query.GetCacheKey()), async () =>
                {
                    await _keywordReplyRepository.GetByQueryAsync(query);
                    return query;
                });
            if (!query.Equals(tempQuery))
            {
                query.RecordCount = tempQuery.RecordCount;
                query.Result = tempQuery.Result;
            }
            var result = query.MapToQueryDto<KeywordReplyQueryViewModel, KeepGoing.Domain.Models.KeywordReply>();
            var Items = Config.SenparcWeixinSetting.Items;
            foreach (var item in result.Result)
            {
                item.imageFile = _fileService.GetUrl(item.imageFile);
                item.txtFile = _fileService.GetUrl(item.txtFile);
                var it = Items.Where(x => x.Value.WeixinAppId == item.AppId).FirstOrDefault();
                item.Count = await _iWXTextMessageRepository.GetCountByWxId(item.AppId, item.KeywordReplyName);
                item.AppName = it.Key;
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="title"></param>
        /// <param name="content"></param>
        /// <param name="KeywordReplyName"></param>
        /// <returns></returns>
        private ImageText HandLine(string title, string content, string KeywordReplyName)
        {
            ImageText imageText = new ImageText();
            string stringBuilder = string.Empty;
            imageText.Title = title;
            stringBuilder += $"<section style='font-size: 13px;font-weight:bold;'>{title}</section>";

            string[] lines = content.Split(new string[] { "\r\n", "\n", "\r" }, StringSplitOptions.None);
            foreach (var item in lines)
            {
                stringBuilder += $"<section style='font-size: 13px;'>{item}</section>";
            }
            stringBuilder += $"<section style='font-size: 12px;color:#ff6b16;'>({KeywordReplyName})</section>";

            //stringBuilder += "<section data-support='96编辑器' data-style-id='19754' style='margin-bottom:unset;'><section style='margin-top:10px;margin-right:0%;margin-left:0%;margin-bottom:unset;'><section style='display: inline-block;width: 100%;border-width: 1px;border-style: solid;border-color: rgb(19, 95, 85);padding: 10px;background-color: rgb(255, 255, 255);border-radius: 10px;box-shadow: rgb(0, 0, 0) 0px 0px 0px;margin-bottom: unset;' data-width='100%'><section style='margin-bottom:unset;'><section style='display:inline-block;vertical-align:middle;width:35%;margin-bottom:unset;' data-width='35%'><p style='text-align:center;'><img class='rich_pages wxw-img' data-cropselx1='0' data-cropselx2='165' data-cropsely1='0' data-cropsely2='165' data-imgfileid='100017186' data-ratio='0.9614767255216693' data-w='623' style='width: 172px !important; vertical-align: bottom; height: auto !important; visibility: visible !important;' data-src='https://mmbiz.qpic.cn/sz_mmbiz_jpg/MvDGJDNXjkzmpjtUADoBpEYJgcLIt1QhicKFseSOITDYUE4SsDckX0FDJrXc5YeOtia6IZJkbYSGMmzmOoJWpzpQ/640?wx_fmt=jpeg&amp;from=appmsg' data-original-style='width: 172px;vertical-align: bottom;height: auto !important;' data-index='2' src='https://mmbiz.qpic.cn/sz_mmbiz_jpg/MvDGJDNXjkzmpjtUADoBpEYJgcLIt1QhicKFseSOITDYUE4SsDckX0FDJrXc5YeOtia6IZJkbYSGMmzmOoJWpzpQ/640?wx_fmt=jpeg&amp;from=appmsg&amp;tp=webp&amp;wxfrom=5&amp;wx_lazy=1&amp;wx_co=1' _width='172px' crossorigin='anonymous' alt='图片' data-fail='0'></p></section><section style='display: inline-block;vertical-align: middle;width: 65%;box-shadow: rgb(0, 0, 0) 0px 0px 0px;padding-left: 5px;margin-bottom: unset;' data-width='65%'><section style='margin-bottom:unset;'><section style='margin-bottom:unset;'><p style='color:#646464;font-size:12px;'><strong><span style='color:#3e6511;font-size:15px;'><strong style='letter-spacing: 0.578px;text-align: center;color: rgb(100, 100, 100);font-size: 12px;background-color: rgb(255, 255, 255);'><span style='color: rgb(62, 101, 17);font-size: 15px;'>喜欢推文的姐妹可以点赞、收藏、转发</span></strong></span></strong></p><p style='color:#646464;font-size:12px;'><strong><span style='color:#3e6511;font-size:15px;'><strong style='letter-spacing: 0.578px;text-align: center;color: rgb(100, 100, 100);font-size: 12px;background-color: rgb(255, 255, 255);'><span style='color: rgb(62, 101, 17);font-size: 15px;'>内容来源于网络，仅供交流</span></strong></span></strong></p></section></section></section></section></section></section></section>";

            //stringBuilder += $"<section style='margin-bottom: unset;'><p style='text-align: center;'><strong style='color: rgb(100, 100, 100);font-size: 12px;letter-spacing: 0.578px;text-wrap: wrap;background-color: rgb(255, 255, 255);'><span style='color: rgb(62, 101, 17);font-size: 15px;'><span style='color: rgb(62, 101, 17);font-size: 15px;font-style: italic;font-weight: 700;letter-spacing: 0.578px;text-wrap: wrap;background-color: rgb(255, 255, 255);'>拜托姐妹们看文记得💧💧</span></span></strong></p><p style='text-align: center;'><font color='#3e6511' face='Optima-Regular, PingFangTC-light'><span style='font-size: 14px;letter-spacing: 2px;'><b><i>{KeywordReplyName}</i></b></span></font></p></section>";

            imageText.Content = stringBuilder.ToString();
            return imageText;
        }

        /// <summary>
        /// 处理内容
        /// </summary>
        /// <param name="str"></param>
        /// <param name="KeywordReplyName"></param>
        /// <returns></returns>
        private ImageText HandLine(string str, string KeywordReplyName)
        {
            ImageText imageText = new ImageText();
            string stringBuilder = string.Empty;
            string[] lines = str.Split("\r\n");
            bool IsTitle = true;
            foreach (string line in lines)
            {
                if (string.IsNullOrEmpty(line))
                {
                    continue;
                }
                if (IsTitle)
                {
                    imageText.Title = line;
                    stringBuilder += $"<section style='font-size: 13px;font-weight:bold;'>{line}</section>";
                    IsTitle = false;
                }
                else
                {
                    stringBuilder += $"<section style='font-size: 13px;'>{line}</section>";
                }
            }
            stringBuilder += $"<section style='font-size: 12px;color:#ff6b16;'>({KeywordReplyName})</section>";
            imageText.Content = stringBuilder.ToString();
            return imageText;
        }

        /// <summary>
        /// 图文消息
        /// </summary>
        private class AppIdContent
        {
            /// <summary>
            /// 微信Id
            /// </summary>
            public string AppId { get; set; }


            /// <summary>
            /// 图文内容
            /// </summary>
            public List<ImageText> imageTexts = new List<ImageText>();

        }
        public class ImageText
        {
            /// <summary>
            /// 标题
            /// </summary>
            public string Title { get; set; }

            /// <summary>
            /// 图文内容
            /// </summary>
            public string Content { get; set; }
        }


        /// <summary>
        /// 创建规则
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ServiceMethodPermissionDescriptor("创建规则", Permission.Post, UserType.AdminUser, SystemModule.BaseModule)]
        public async Task<string> AppCreateAsync([FromBody] KeywordReplyCreateModel model)
        {
            string msg = string.Empty;

            List<AppIdContent> appIdContents = new List<AppIdContent>();
            if (string.IsNullOrEmpty(model.txtFile) && model.ArticleData.Any())
            {
                List<string> strList = new List<string>();
                List<string> AppIds = model.appId.ToList();

                List<ImageText> imageTexts = new List<ImageText>();

                foreach (var AppId in AppIds)
                {
                    model.imageFile = await _keywordReplyRepository.GetImageFileByAppId(AppId);

                    foreach (var item in model.ArticleData)
                    {
                        imageTexts.Add(HandLine(item.title, item.content, model.KeywordReplyName));
                    }
                    appIdContents.Add(new AppIdContent() { AppId = AppId, imageTexts = imageTexts });
                    imageTexts = new List<ImageText>();
                    strList.Clear();
                }
            }
            Console.WriteLine(model.imageFile);

            foreach (var item in model.appId)
            {
                var draft = await draftadd(item, model.imageFile, appIdContents);
                if (string.IsNullOrEmpty(draft.errmsg))
                {
                    if (string.IsNullOrEmpty(model.imageFile))
                    {
                        model.imageFile = "default";
                    }
                    if (string.IsNullOrEmpty(model.txtFile))
                    {
                        model.txtFile = "default";
                    }
                    var command = new CreateKeywordReplyCommand(item, model.RuleName, model.MateType, model.KeywordReplyName, model.ReplyContent, model.imageFile, model.txtFile);
                    await _bus.SendCommand(command);
                    if (_notifications.HasNotifications())
                    {
                        var errorMessage = _notifications.GetNotificationMessage();
                        msg = JsonSerializer.Serialize(errorMessage);
                    }
                    else
                    {
                        msg = "添加成功";
                    }

                }
                else
                {
                    msg = draft.errmsg;
                }
            }
            return msg;
        }


        /// <summary>
        /// 创建规则
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ServiceMethodPermissionDescriptor("创建规则", Permission.Post, UserType.AdminUser, SystemModule.BaseModule)]
        public async Task<string> CreateAsync([FromBody] KeywordReplyCreateModel model)
        {
            string msg = string.Empty;

            List<AppIdContent> appIdContents = new List<AppIdContent>();
            if (!string.IsNullOrEmpty(model.txtFile))
            {
                string filePath = _fileService.GetFile(model.txtFile);
                string content = File.ReadAllText(filePath);
                //按2个换行进行拆分
                string[] lines = content.Split("\r\n\r\n");

                List<string> strList = new List<string>();
                List<string> AppIds = model.appId.ToList();

                List<ImageText> imageTexts = new List<ImageText>();

                foreach (var AppId in AppIds)
                {
                    for (int Index = 0; Index < lines.Length; Index++)
                    {
                        string line = lines[Index];
                        imageTexts.Add(HandLine(line, model.KeywordReplyName));
                    }
                    appIdContents.Add(new AppIdContent() { AppId = AppId, imageTexts = imageTexts });
                    imageTexts = new List<ImageText>();
                    strList.Clear();
                }
            }

            foreach (var item in model.appId)
            {
                var draft = await draftadd(item, model.imageFile, appIdContents);
                if (string.IsNullOrEmpty(draft.errmsg))
                {
                    if (string.IsNullOrEmpty(model.imageFile))
                    {
                        model.imageFile = "default";
                    }
                    if (string.IsNullOrEmpty(model.txtFile))
                    {
                        model.txtFile = "default";
                    }
                    var command = new CreateKeywordReplyCommand(item, model.RuleName, model.MateType, model.KeywordReplyName, model.ReplyContent, model.imageFile, model.txtFile);
                    await _bus.SendCommand(command);
                    if (_notifications.HasNotifications())
                    {
                        var errorMessage = _notifications.GetNotificationMessage();
                        throw new GirvsException(StatusCodes.Status400BadRequest, errorMessage);
                    }
                    msg = "添加成功";
                }
                else
                {
                    msg = draft.errmsg;
                }
            }
            return msg;
        }


        /// <summary>
        /// 修改规则
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <exception cref="GirvsException"></exception>
        [HttpPut("{Id}")]
        [ServiceMethodPermissionDescriptor("修改规则", Permission.Edit_Extend1)]
        public async Task UpdateAsync(Guid Id, [FromBody] KeywordReplyEditModel model)
        {
            var command = new UpdataKeywordReplyCommand(Id, model.RuleName, model.MateType, model.KeywordReplyName, model.ReplyContent);

            await _bus.SendCommand(command);
            if (_notifications.HasNotifications())
            {
                var errorMessage = _notifications.GetNotificationMessage();
                throw new GirvsException(StatusCodes.Status400BadRequest, errorMessage);
            }
        }


        /// <summary>
        /// 删除规则
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        /// <exception cref="GirvsException"></exception>
        [HttpDelete("{Id}")]
        [ServiceMethodPermissionDescriptor("删除规则", Permission.Edit_Extend1)]
        public async Task DeleteAsync(Guid Id)
        {
            var command = new DeleteKeywordReplyCommand(Id);

            await _bus.SendCommand(command);
            if (_notifications.HasNotifications())
            {
                var errorMessage = _notifications.GetNotificationMessage();
                throw new GirvsException(StatusCodes.Status400BadRequest, errorMessage);
            }
        }

        /// <summary>
        /// 获取匹配内容
        /// </summary>
        /// <param name="appId"></param>
        /// <param name="keywordReplyName"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        [NonAction]
        public string? GetReplyContent(string appId, string keywordReplyName)
        {
            var list = _keywordReplyRepository.GetWhereAsync(x => x.AppId == appId && ((x.KeywordReplyName.Contains(keywordReplyName) && x.MateType == MateTypeEnum.PartialMatching) || (x.KeywordReplyName == keywordReplyName && x.MateType == MateTypeEnum.PerfectMatch))).Result;
            return list.FirstOrDefault()?.ReplyContent;
        }

        /// <summary>
        /// 新建草稿
        /// </summary>
        /// <returns></returns>
        private async Task<AddDraftResultJson> draftadd(string modelAppId, string imageFile, List<AppIdContent> appIdContents)
        {
            AppIdContent appIdContent = appIdContents.Where(x => x.AppId == modelAppId).FirstOrDefault();

            if (string.IsNullOrEmpty(imageFile) || !appIdContent.imageTexts.Any())
            {
                return new AddDraftResultJson { };
            }
            var image = UploadForeverMedia(imageFile, modelAppId);

            List<DraftModel> draftModels = new List<DraftModel>();
            foreach (ImageText imageText in appIdContent.imageTexts)
            {
                DraftModel draftModel = new DraftModel
                {
                    content = imageText.Content,
                    title = imageText.Title,
                    show_cover_pic = "0",
                    need_open_comment = 0,
                    thumb_media_id = image
                };
                draftModels.Add(draftModel);
            }
            var result = await Senparc.Weixin.MP.AdvancedAPIs.Draft.DraftApi.AddDraftAsync(modelAppId, 10000, draftModels.Where(x => !string.IsNullOrEmpty(x.title)).ToArray());
            return result;
        }

        /// <summary>
        /// 上传文件
        /// </summary>
        /// <returns></returns>
        private string UploadForeverMedia(string fileId, string modelAppId)
        {
            try
            {
                //var key = new CacheKey(fileId);
                //return _cacheManager.Get(key, () =>
                //{
                string file = _fileService.GetFile(fileId);
                var resl = Senparc.Weixin.MP.AdvancedAPIs.MediaApi.UploadForeverMedia(modelAppId, file, UploadForeverMediaType.image);
                return resl.media_id;
                //});
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
