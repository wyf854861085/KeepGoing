﻿using Girvs.AuthorizePermission;
using Girvs.Infrastructure;
using KeepGoing.Application.ViewModels.WeiXin;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Panda.DynamicWebApi.Attributes;
using Senparc.NeuChar.MessageHandlers;
using Senparc.Weixin.MP;
using Senparc.Weixin;
using Senparc.Weixin.MP.Entities.Request;
using Senparc.Weixin.Entities;
using KeepGoing.Application.ViewModels;
using KeepGoing.Application.AppService.KeywordReply;
using Girvs.Extensions;
using Senparc.Weixin.MP.AdvancedAPIs.Draft.DraftJson;
using Senparc.Weixin.MP.Containers;
using System.Text;

namespace KeepGoing.Application.AppService.WeiXin
{
    /// <summary>
    /// 角色管理接口
    /// </summary>
    [DynamicWebApi]
    [Authorize(AuthenticationSchemes = GirvsAuthenticationScheme.GirvsJwt)]
    [ApiController]
    [Route("api/WeiXinXtj")]
    [ServicePermissionDescriptor("微信管理", "916DE9AF-F3FA-4D0B-BF4F-ED796695B182", "系统信息管理", SystemModule.BaseModule, 3)]
    public class XtJWeiXinAppService : IXtJWeiXinAppService
    {
        public const string WxName = "小胖吴的小铁匠";


        public static readonly string Token = Config.SenparcWeixinSetting.Items[WxName].Token;//与微信公众账号后台的Token设置保持一致，区分大小写。
        public static readonly string EncodingAESKey = Config.SenparcWeixinSetting.Items[WxName].EncodingAESKey;//与微信公众账号后台的EncodingAESKey设置保持一致，区分大小写。
        public static readonly string AppId = Config.SenparcWeixinSetting.Items[WxName].WeixinAppId;//与微信公众账号后台的AppId设置保持一致，区分大小写。

        readonly Func<string> _getRandomFileName = () => SystemTime.Now.ToString("yyyyMMdd-HHmmss") + Guid.NewGuid().ToString("n").Substring(0, 6);


        private readonly IKeywordReplyAppService _keywordReplyAppService;

        public XtJWeiXinAppService(IKeywordReplyAppService keywordReplyAppService)
        {
            _keywordReplyAppService = keywordReplyAppService;
        }

        /// <summary>
        /// 获取微信公众号集合
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("GetAppIds")]
        public List<SelectModel> GetAppIds()
        {
            //var list = Senparc.Weixin.MP.AdvancedAPIs.Draft.DraftApi.GetDraftList(AppId, 0, 20);

            List<SelectModel> selectModels = new List<SelectModel>();
            var Items = Config.SenparcWeixinSetting.Items;
            foreach (var item in Items)
            {
                if (!selectModels.Any(x => x.key == item.Value.WeixinAppId))
                    selectModels.Add(new SelectModel
                    {
                        value = item.Key,
                        key = item.Value.WeixinAppId
                    });
            }
            return selectModels;
        }

        /// <summary>
        /// 微信通过get请求验证api接口
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public string GetToken(string signature, string timestamp, string nonce, string echostr)
        {
            Console.WriteLine(Token);
            if (CheckSignature.Check(signature, timestamp, nonce, Token))
            {
                return echostr; //返回随机字符串则表示验证通过
            }
            else
            {
                return "failed:" + signature + "," + CheckSignature.GetSignature(timestamp, nonce, Token) + "。" +
                    "如果你在浏览器中看到这句话，说明此地址可以被作为微信公众账号后台的Url，请注意保持Token一致。";
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public string RequestMsg()
        {
            StreamReader stream = new StreamReader(EngineContext.Current.HttpContext.Request.Body);
            string messageStr = stream.ReadToEndAsync().GetAwaiter().GetResult();
            var message = OfficialAccount.ReceivingStandardMessages.Parse(messageStr);
            if (message is OfficialAccount.ReceivingStandardMessages.TextMessage)
            {
                var textMessage = (OfficialAccount.ReceivingStandardMessages.TextMessage)message;
                Console.WriteLine(textMessage.Content);
                var content = textMessage.Content;
                var replyMessage = new OfficialAccount.PassiveUserReplyMessage.TextMessage
                {
                    Content = "你好，很高兴认识你、欢迎关注我的公众号~",
                    CreateTime = DateTimeOffset.Now.ToUnixTimeMilliseconds(),
                    ToUserName = message.FromUserName,
                    FromUserName = message.ToUserName,
                    MsgType = message.MsgType,
                };
                if (content != null)
                {
                    var result = _keywordReplyAppService.GetReplyContent(AppId, content);
                    if (!result.IsNullOrEmpty())
                    {
                        replyMessage.Content = result;
                    }
                    else
                    {
                        replyMessage.Content = "没有匹配到对应的关键字,请重新输入!";
                    }

                }
                return OfficialAccount.PassiveUserReplyMessage.Parse(replyMessage);
            }
            return "hello, this is handle view";
        }
    }
}
