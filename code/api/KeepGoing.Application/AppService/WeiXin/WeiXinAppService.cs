﻿using Girvs.AuthorizePermission;
using Girvs.Infrastructure;
using KeepGoing.Application.ViewModels.WeiXin;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Panda.DynamicWebApi.Attributes;
using Senparc.NeuChar.MessageHandlers;
using Senparc.Weixin.MP;
using Senparc.Weixin;
using Senparc.Weixin.MP.Entities.Request;
using Senparc.Weixin.Entities;
using KeepGoing.Application.ViewModels;
using KeepGoing.Application.AppService.KeywordReply;
using Girvs.Extensions;
using Senparc.Weixin.MP.AdvancedAPIs.Draft.DraftJson;
using Senparc.Weixin.MP.Containers;
using System.Text;
using Girvs;
using KeepGoing.Domain.Commands.KeywordReply;
using MediatR;
using Microsoft.AspNetCore.Http;
using KeepGoing.Domain.Commands.WXTextMessage;
using Girvs.Driven.Bus;
using Girvs.Driven.Notifications;
using KeepGoing.Infrastructure.Repositories;
using KeepGoing.Domain.Repositories;
using KeepGoing.Domain.Models;
using KeepGoing.Application.ViewModels.KeywordReply;
using System.Collections.Generic;
using KeepGoing.Domain.Extensions;
using System.Text.Json;
using static Aliyun.Api.LogService.Infrastructure.Serialization.Protobuf.Log.Types;

namespace KeepGoing.Application.AppService.WeiXin
{
    /// <summary>
    /// 角色管理接口
    /// </summary>
    [DynamicWebApi]
    [Authorize(AuthenticationSchemes = GirvsAuthenticationScheme.GirvsJwt)]
    [ApiController]
    [Route("api/WeiXin")]
    [ServicePermissionDescriptor("微信管理", "42d01236-283a-4368-975b-b8fdb77dd6a4", "系统信息管理", SystemModule.BaseModule, 3)]
    public class WeiXinAppService : IWeiXinAppService
    {
        public const string WxName = "dotnet成长";


        public static readonly string Token = Config.SenparcWeixinSetting.Items[WxName].Token;//与微信公众账号后台的Token设置保持一致，区分大小写。
        public static readonly string EncodingAESKey = Config.SenparcWeixinSetting.Items[WxName].EncodingAESKey;//与微信公众账号后台的EncodingAESKey设置保持一致，区分大小写。
        public static readonly string AppId = Config.SenparcWeixinSetting.Items[WxName].WeixinAppId;//与微信公众账号后台的AppId设置保持一致，区分大小写。

        readonly Func<string> _getRandomFileName = () => SystemTime.Now.ToString("yyyyMMdd-HHmmss") + Guid.NewGuid().ToString("n").Substring(0, 6);


        private readonly IKeywordReplyAppService _keywordReplyAppService;

        private readonly IMediatorHandler _bus;

        private readonly DomainNotificationHandler _notifications;

        private readonly IIWXTextMessageRepository _iWXTextMessageRepository;

        private readonly IKeywordReplyRepository _keywordReplyRepository;

        public WeiXinAppService(IKeywordReplyAppService keywordReplyAppService, IMediatorHandler bus, INotificationHandler<DomainNotification> notifications, IIWXTextMessageRepository iWXTextMessageRepository, IKeywordReplyRepository keywordReplyRepository)
        {
            _keywordReplyAppService = keywordReplyAppService;
            _bus = bus;
            _notifications = (DomainNotificationHandler)notifications; ;
            _iWXTextMessageRepository = iWXTextMessageRepository;
            _keywordReplyRepository = keywordReplyRepository;
        }

        /// <summary>
        /// 获取微信公众号集合
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("GetAppIds")]
        public List<SelectModel> GetAppIds()
        {
            //var list = Senparc.Weixin.MP.AdvancedAPIs.Draft.DraftApi.GetDraftList(AppId, 0, 20);

            List<SelectModel> selectModels = new List<SelectModel>();
            var Items = Config.SenparcWeixinSetting.Items;

            foreach (var item in Items)
            {
                if (!selectModels.Any(x => x.key == item.Value.WeixinAppId))
                    selectModels.Add(new SelectModel
                    {
                        value = item.Key,
                        key = item.Value.WeixinAppId
                    });
            }
            return selectModels;
        }

        /// <summary>
        /// 微信通过get请求验证api接口
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public string GetToken(string signature, string timestamp, string nonce, string echostr)
        {
            if (CheckSignature.Check(signature, timestamp, nonce, Token))
            {
                return echostr; //返回随机字符串则表示验证通过
            }
            else
            {
                return "failed:" + signature + "," + CheckSignature.GetSignature(timestamp, nonce, Token) + "。" +
                    "如果你在浏览器中看到这句话，说明此地址可以被作为微信公众账号后台的Url，请注意保持Token一致。";
            }
        }
        [NonAction]
        public async Task<string> ReceiveMessage(string user, string content)
        {
            var replyMessage = new OfficialAccount.PassiveUserReplyMessage.TextMessage
            {
                Content = "你好，很高兴认识你、欢迎关注我的公众号~",
                CreateTime = DateTimeOffset.Now.ToUnixTimeMilliseconds(),
            };
            if (!string.IsNullOrEmpty(content))
            {
                if (content == "发文")
                {
                    replyMessage.Content = "收到,请发送你需要发表的文章内容!";
                }
                else if (content.ToUpper() == "OK")
                {

                    List<WXTextMessage> wXTextMessages = await _iWXTextMessageRepository.GetByWxIdFromUserName(AppId, user);
                    KeywordReplyCreateModel keywordReplyCreateModel = new KeywordReplyCreateModel();
                    keywordReplyCreateModel.appId = new List<string> { AppId };
                    keywordReplyCreateModel.RuleName = DateTime.Now.ToString("MMdd");
                    keywordReplyCreateModel.MateType = MateTypeEnum.PartialMatching;
                    keywordReplyCreateModel.ReplyContent = wXTextMessages.LastOrDefault()?.Content;

                    string? KeywordReplyName = await _iWXTextMessageRepository.GetRandText();

                    while (await _keywordReplyRepository.GetKeywordReplyNameAny(AppId, KeywordReplyName))
                    {
                        KeywordReplyName = await _iWXTextMessageRepository.GetRandText();
                    }
                    keywordReplyCreateModel.KeywordReplyName = KeywordReplyName;
                    List<ArticleData> articleDatas = new List<ArticleData>();

                    if (wXTextMessages.Count > 0)
                    {
                        wXTextMessages.RemoveRange(wXTextMessages.Count - 1, 1);
                    }
                    while (wXTextMessages.Any())
                    {
                        ArticleData articleData = new ArticleData();
                        WXTextMessage? wXTextMessage = wXTextMessages.FirstOrDefault();
                        if (wXTextMessage != null)
                        {
                            articleData.title = wXTextMessage.Content;
                            wXTextMessages.Remove(wXTextMessage);
                        }
                        if (wXTextMessages != null)
                        {
                            wXTextMessage = wXTextMessages.FirstOrDefault();
                            if (wXTextMessage != null)
                            {
                                articleData.content = wXTextMessage.Content;
                                wXTextMessages.Remove(wXTextMessage);
                            }
                        }
                        articleDatas.Add(articleData);

                    }
                    keywordReplyCreateModel.ArticleData = articleDatas;

                    Console.WriteLine(JsonSerializer.Serialize(keywordReplyCreateModel));
                    await _keywordReplyAppService.AppCreateAsync(keywordReplyCreateModel);
                    replyMessage.Content = $"创建草稿成功!\r\n共{articleDatas.Count()}篇文章\r\n关键词:{KeywordReplyName}\r\n回复:{keywordReplyCreateModel.ReplyContent}\r\n请在微信助手中发布!";

                    return replyMessage.Content;
                }
                else
                {
                    replyMessage.Content = "收到,请继续输入!";
                }
                await _iWXTextMessageRepository.AddWXTextMessage(AppId, content, replyMessage.Content, user);
                return replyMessage.Content;
            }
            return replyMessage.Content;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<string> RequestMsg()
        {
            StreamReader stream = new StreamReader(EngineContext.Current.HttpContext.Request.Body);
            string messageStr = stream.ReadToEndAsync().GetAwaiter().GetResult();
            var message = OfficialAccount.ReceivingStandardMessages.Parse(messageStr);
            if (message is OfficialAccount.ReceivingStandardMessages.TextMessage)
            {
                var textMessage = (OfficialAccount.ReceivingStandardMessages.TextMessage)message;
                var content = textMessage.Content;
                var replyMessage = new OfficialAccount.PassiveUserReplyMessage.TextMessage
                {
                    Content = "你好，很高兴认识你、欢迎关注我的公众号~",
                    CreateTime = DateTimeOffset.Now.ToUnixTimeMilliseconds(),
                    ToUserName = message.FromUserName,
                    FromUserName = message.ToUserName,
                    MsgType = message.MsgType,
                };

                List<string> FromUserNameList = new List<string>() { "o6OLCw7qL5b4SZPSta-O_GdTYeJg", "o6OLCw0GeERe9HYBaL8oF8d5zKc8" };

                content = content?.TrimStart().TrimEnd();
                if (content != null)
                {
                    var result = _keywordReplyAppService.GetReplyContent(AppId, content);
                    if (!result.IsNullOrEmpty())
                    {
                        replyMessage.Content = result;
                    }
                    else
                    {
                        if (FromUserNameList.Any(x => x == message.FromUserName))
                        {
                            if (content == "发文")
                            {
                                replyMessage.Content = "收到,请发送你需要发表的文章内容!";
                            }
                            else if (content.ToUpper() == "OK")
                            {
                                List<WXTextMessage> wXTextMessages = await _iWXTextMessageRepository.GetByWxIdFromUserName(AppId, message.FromUserName);
                                KeywordReplyCreateModel keywordReplyCreateModel = new KeywordReplyCreateModel();
                                keywordReplyCreateModel.appId = new List<string> { AppId };
                                keywordReplyCreateModel.RuleName = DateTime.Now.ToString("MMdd");
                                keywordReplyCreateModel.MateType = MateTypeEnum.PartialMatching;
                                keywordReplyCreateModel.ReplyContent = wXTextMessages.LastOrDefault()?.Content;

                                string? KeywordReplyName = await _iWXTextMessageRepository.GetRandText();

                                while (await _keywordReplyRepository.GetKeywordReplyNameAny(AppId, KeywordReplyName))
                                {
                                    KeywordReplyName = await _iWXTextMessageRepository.GetRandText();
                                }
                                keywordReplyCreateModel.KeywordReplyName = KeywordReplyName;
                                List<ArticleData> articleDatas = new List<ArticleData>();

                                if (wXTextMessages.Count > 0)
                                {
                                    wXTextMessages.RemoveRange(wXTextMessages.Count - 1, 1);
                                }
                                while (wXTextMessages.Any())
                                {
                                    ArticleData articleData = new ArticleData();
                                    WXTextMessage? wXTextMessage = wXTextMessages.FirstOrDefault();
                                    if (wXTextMessage != null)
                                    {
                                        articleData.title = wXTextMessage.Content;
                                        wXTextMessages.Remove(wXTextMessage);
                                    }
                                    if (wXTextMessages != null)
                                    {
                                        wXTextMessage = wXTextMessages.FirstOrDefault();
                                        if (wXTextMessage != null)
                                        {
                                            articleData.content = wXTextMessage.Content;
                                            wXTextMessages.Remove(wXTextMessage);
                                        }
                                    }
                                    articleDatas.Add(articleData);

                                }
                                keywordReplyCreateModel.ArticleData = articleDatas;

                                Console.WriteLine(JsonSerializer.Serialize(keywordReplyCreateModel));
                                await _keywordReplyAppService.AppCreateAsync(keywordReplyCreateModel);
                                replyMessage.Content = $"创建草稿成功!\r\n共{articleDatas.Count()}篇文章\r\n关键词:{KeywordReplyName}\r\n回复:{keywordReplyCreateModel.ReplyContent}\r\n请在微信助手中发布!";

                                return OfficialAccount.PassiveUserReplyMessage.Parse(replyMessage);
                            }
                            else
                            {
                                replyMessage.Content = "收到,请继续输入!";
                            }
                        }
                        else
                        {
                            replyMessage.Content = "没有匹配到对应的关键字,请重新输入!";
                        }
                    }
                }
                var command = new CreateWXTextMessageCommand(AppId, content, message.ToUserName, message.FromUserName, message.MsgType, message.MsgId, message.MsgDataId, message.Idx, replyMessage.Content);
                await _bus.SendCommand(command);
                if (_notifications.HasNotifications())
                {
                    var errorMessage = _notifications.GetNotificationMessage();
                    Console.Write(errorMessage);
                }

                return OfficialAccount.PassiveUserReplyMessage.Parse(replyMessage);
            }
            return "hello, this is handle view";
        }
    }
}
