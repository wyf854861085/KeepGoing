﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Application.ViewModels.User
{
    /// <summary>
    /// 用户登录输入实体
    /// </summary>
    public class UserLoginModel
    {
        /// <summary>
        /// 用户名称
        /// </summary>
        [Required]
        public string UserAccount { get; set; }

        /// <summary>
        /// 用户登陆密码
        /// </summary>
        [Required]
        public string Password { get; set; }


        /// <summary>
        /// 验证码不能为空
        /// </summary>
        [Required(ErrorMessage = "验证码不能为空")]
        public string VerifiCode { get; set; }

        /// <summary>
        /// 验证码
        /// </summary>
        [Required]
        public string Guid { get; set; }
    }
}
