﻿using Girvs.AutoMapper.Mapper;
using Girvs.BusinessBasis.Dto;
using Girvs.Extensions.Collections;
using KeepGoing.Domain.Extensions;
using KeepGoing.Domain.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Application.ViewModels.User
{

    /// <summary>
    /// 用户列表插入输入
    /// </summary>
    [AutoMapFrom(typeof(UserQuery))]
    [AutoMapTo(typeof(UserQuery))]
    public class UserQueryViewModel : QueryDtoBase<UserQueryListViewModel>
    {
        /// <summary>
        /// 用户名称
        /// </summary>

        public string? UserName { get; set; }
    }

    /// <summary>
    /// 用户列表查询返回
    /// </summary>
    [AutoMapFrom(typeof(Domain.Models.User))]
    public class UserQueryListViewModel : IDto
    {

        public Guid Id { get; set; }

        /// <summary>
        /// 用户名称
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 登录账号
        /// </summary>
        public string UserAccount { get; set; }

        /// <summary>
        /// 用户状态
        /// </summary>
        public UserState UserState { get; set; }

        /// <summary>
        /// 用户状态名称
        /// </summary>
        public string UserStateName { get { return UserState.GetEnumDescription(); } }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime { get; set; }

    }
}

