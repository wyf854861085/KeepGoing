﻿using Girvs.AutoMapper.Mapper;
using Girvs.BusinessBasis.Dto;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace KeepGoing.Application.ViewModels.User
{

    [AutoMapFrom(typeof(Domain.Models.User))]
    public class UserEditViewModel : IDto
    {
        [DisplayName("用户名称")]
        [Required(ErrorMessage = "{0}不能为空")]
        [MinLength(1, ErrorMessage = "{0}最小长度为1")]
        [MaxLength(30, ErrorMessage = "{0}最大长度为30")]
        public string UserName { get; set; }


        [DisplayName("登录名称")]
        [Required(ErrorMessage = "{0}不能为空")]
        [MinLength(1, ErrorMessage = "{0}最小长度为1")]
        [MaxLength(30, ErrorMessage = "{0}最大长度为30")]
        public string UserAccount { get; set; }


        [DisplayName("登录密码")]
        [Required(ErrorMessage = "{0}不能为空")]
        [MaxLength(200, ErrorMessage = "{0}最大长度为200")]
        public string UserPassWord { get; set; }


        [DisplayName("用户联系方式")]
        [MaxLength(30, ErrorMessage = "{0}最大长度为30")]
        public string? ContactNumber { get; set; }

    }

    [AutoMapFrom(typeof(Domain.Models.User))]
    public class EditUserNameViewModel : IDto
    {
        [DisplayName("用户名称")]
        [Required(ErrorMessage = "{0}不能为空")]
        [MinLength(1, ErrorMessage = "{0}最小长度为1")]
        [MaxLength(30, ErrorMessage = "{0}最大长度为30")]
        public string UserName { get; set; }

    }
}
