﻿using Girvs.AuthorizePermission.Enumerations;
using Girvs.AutoMapper.Mapper;
using Girvs.BusinessBasis.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Application.ViewModels.User
{
    [AutoMapFrom(typeof(Domain.Models.User))]
    [AutoMapTo(typeof(Domain.Models.User))]
    public class UserInfoModel:IDto
    {
        /// <summary>
        /// 登录名称
        /// </summary>
        public string UserAccount { get; set; }

        /// <summary>
        /// 用户名称
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 用户联系方式
        /// </summary>
        public string? ContactNumber { get; set; }

        /// <summary>
        /// 用户类型
        /// </summary>
        public UserType UserType { get; set; }
    }
}
