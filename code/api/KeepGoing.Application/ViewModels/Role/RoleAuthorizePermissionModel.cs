﻿using Girvs.BusinessBasis.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Application.ViewModels.Role
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="ServiceId">模块Id</param>
    /// <param name="checkedData">设置权限</param>
    public record RoleAuthorizePermissionModel(Guid ServiceId, List<int> checkedData) : IDto;
}
