﻿using Girvs.AutoMapper.Mapper;
using Girvs.BusinessBasis.Dto;
using KeepGoing.Domain.Queries;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Application.ViewModels.Role
{
    [AutoMapFrom(typeof(RoleQuery))]
    [AutoMapTo(typeof(RoleQuery))]
    public class RoleQueryViewModel : QueryDtoBase<RoleQueryListViewModel>
    {
        /// <summary>
        /// 角色名称
        /// </summary>

        public string? RoleName { get; set; }
    }
    [AutoMapFrom(typeof(Domain.Models.Role))]
    public class RoleQueryListViewModel : IDto
    {

        public Guid Id { get; set; }

        /// <summary>
        /// 角色名称
        /// </summary>
        public string RoleName { get; set; }

        /// <summary>
        /// 角色描述
        /// </summary>
        public string RoleDesc { get; set; }

        /// <summary>
        /// 排序号
        /// </summary>
        public int Sort { get; set; }

    }

    [AutoMapFrom(typeof(Domain.Models.Role))]
    public class RoleAllListViewModel : IDto
    {

        public Guid Id { get; set; }

        /// <summary>
        /// 角色名称
        /// </summary>
        public string RoleName { get; set; }

    }
}

