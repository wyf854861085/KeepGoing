﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Application.ViewModels.Role
{
    /// <summary>
    /// 创建角色
    /// </summary>
    public class RoleCreateModel
    {
        [DisplayName("角色名称")]
        [Required(ErrorMessage = "{0}不能为空")]
        [MinLength(1, ErrorMessage = "{0}最小长度为1")]
        [MaxLength(30, ErrorMessage = "{0}最大长度为30")]
        public string RoleName { get; set; }

        [DisplayName("角色描述")]
        [Required(ErrorMessage = "{0}不能为空")]
        [MinLength(1, ErrorMessage = "{0}最小长度为1")]
        [MaxLength(100, ErrorMessage = "{0}最大长度为100")]
        public string RoleDesc { get; set; }

        /// <summary>
        /// 排序号
        /// </summary>
        [DisplayName("排序号")]
        [Required(ErrorMessage = "{0}不能为空")]
        public int Sort { get; set; }
    }
}
