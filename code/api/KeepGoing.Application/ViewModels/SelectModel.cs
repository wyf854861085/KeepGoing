﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Application.ViewModels
{
    public class SelectModel
    {
        public string key { get; set; }

        public string value { get; set; }
    }
}
