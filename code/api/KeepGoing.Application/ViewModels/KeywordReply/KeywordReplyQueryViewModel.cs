﻿using Girvs.AutoMapper.Mapper;
using Girvs.BusinessBasis.Dto;
using Girvs.Extensions.Collections;
using KeepGoing.Domain.Extensions;
using KeepGoing.Domain.Queries;

namespace KeepGoing.Application.ViewModels.KeywordReply
{
    /// <summary>
    /// 规则查询输入模型
    /// </summary>
    [AutoMapFrom(typeof(KeywordReplyQuery))]
    [AutoMapTo(typeof(KeywordReplyQuery))]
    public class KeywordReplyQueryViewModel : QueryDtoBase<KeywordReplyQueryListViewModel>
    {
        /// <summary>
        /// 规则名称
        /// </summary>

        public string? RuleName { get; set; }

        /// <summary>
        /// 微信AppID
        /// </summary>

        public string? AppId { get; set; }

        /// <summary>
        /// 关键字内容
        /// </summary>

        public string? KeywordReplyName { get; set; }
    }

    /// <summary>
    /// 规则查询返回模型
    /// </summary>
    [AutoMapFrom(typeof(Domain.Models.KeywordReply))]
    public class KeywordReplyQueryListViewModel : IDto
    {
        /// <summary>
        /// 规则Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 公众号Id
        /// </summary>
        public string? AppId { get; set; }

        /// <summary>
        /// 公众号名称
        /// </summary>
        public string? AppName { get; set; }

        /// <summary>
        /// 规则名称
        /// </summary>
        public string? RuleName { get; set; }

        /// <summary>
        /// 关键字匹配类型
        /// </summary>
        public MateTypeEnum MateType { get; set; }

        /// <summary>
        /// 关键字匹配类型
        /// </summary>
        public string MateTypeName { get { return MateType.GetEnumDescription(); } }

        /// <summary>
        /// 关键字内容
        /// </summary>
        public string? KeywordReplyName { get; set; }

        /// <summary>
        /// 回复内容
        /// </summary>
        public string? ReplyContent { get; set; }


        /// <summary>
        /// 封面路径
        /// </summary>
        public string imageFile { get; set; }

        /// <summary>
        /// 文本路径
        /// </summary>
        public string txtFile { get; set; }


        /// <summary>
        /// 请求次数
        /// </summary>
        public int Count { get; set; }
    }
}
