﻿using Girvs.BusinessBasis.Dto;
using KeepGoing.Domain.Extensions;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace KeepGoing.Application.ViewModels.KeywordReply
{
    /// <summary>
    /// 创建规则模型
    /// </summary>
    public class KeywordReplyCreateModel : IDto
    {
        [DisplayName("公众号Id")]
        [Required(ErrorMessage = "{0}不能为空")]
        public List<string> appId { get; set; }


        [DisplayName("规则名称")]
        [Required(ErrorMessage = "{0}不能为空")]
        [MaxLength(30, ErrorMessage = "{0}最大长度为30")]
        public string RuleName { get; set; }


        [DisplayName("关键字匹配类型")]
        [Required(ErrorMessage = "{0}不能为空")]
        public MateTypeEnum MateType { get; set; }


        [DisplayName("关键字内容")]
        [Required(ErrorMessage = "{0}不能为空")]
        public string KeywordReplyName { get; set; }

        [DisplayName("回复内容")]
        [Required(ErrorMessage = "{0}不能为空")]
        public string ReplyContent { get; set; }


        /// <summary>
        /// 封面路径
        /// </summary>
        public string? imageFile { get; set; }

        /// <summary>
        /// 文本路径
        /// </summary>
        public string? txtFile { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<ArticleData> ArticleData { get; set; }=new List<ArticleData>();

    }

    public class ArticleData
    {
        public string title { get; set; }

        public string content { get; set; }
    }


}
