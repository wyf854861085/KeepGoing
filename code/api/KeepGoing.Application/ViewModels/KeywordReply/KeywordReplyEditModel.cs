﻿using Girvs.BusinessBasis.Dto;
using KeepGoing.Domain.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepGoing.Application.ViewModels.KeywordReply
{
    /// <summary>
    /// 修改规则模型
    /// </summary>
    public class KeywordReplyEditModel : IDto
    {
        [DisplayName("规则名称")]
        [Required(ErrorMessage = "{0}不能为空")]
        [MaxLength(30, ErrorMessage = "{0}最大长度为30")]
        public string RuleName { get; set; }


        [DisplayName("关键字匹配类型")]
        [Required(ErrorMessage = "{0}不能为空")]
        public MateTypeEnum MateType { get; set; }


        [DisplayName("关键字内容")]
        [Required(ErrorMessage = "{0}不能为空")]
        public string KeywordReplyName { get; set; }


        [DisplayName("回复内容")]
        [Required(ErrorMessage = "{0}不能为空")]
        public string ReplyContent { get; set; }
    }
}
