const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
module.exports = {
    devServer: {
        open: true,
        host: 'localhost',
        port: 5018,
        https: false,
        hotOnly: false,
        // http 代理配置
        proxy: {
            '/api': {
                target: 'api',
                changeOrigin: true,
                pathRewrite: {
                    '^/api': '/'
                }
            }
        },
        before: (app) => { }
    },
    configureWebpack: {
        optimization: {
            minimizer: [new UglifyJsPlugin()]
        }
    }
}

