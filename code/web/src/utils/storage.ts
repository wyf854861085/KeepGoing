// 封装本地存储操作模块方便后续其他页面操作
// 存储数据
export const setItem = (key: string, value: any) => {
    // 将数组对象类型的数据转换为json格式的字符串进行存储
    if (typeof value === "object") {
        // 如果传入的值是对象格式则转换成json字符串格式
        value = JSON.stringify(value);
    }
    // 转换成json字符串后存到localStorage本地，如果如果传入的值是json字符串格式直接存到本地储存
    window.localStorage.setItem(key, value);
};
  
  // 获取数据
export const getItem = (key: string) => {
    const data = window.localStorage.getItem(key); // 从localStorage取出data数据（可能是json字符串也可能是普通字符串）
    try {
        if (data != null)
            // 普通字符串提供JSON.parse转换会报错
            return JSON.parse(data); // 如果data是一个有效的json字符串则执行try里面的转化对象操作
    } catch (err) {
        // 如果data不是一个有效的json字符串（可能是普通字符串），直接获取
        return data;
    }
};
  // 删除数据
export const removeItem = (key: any) => {
    window.localStorage.removeItem(key);
};
  