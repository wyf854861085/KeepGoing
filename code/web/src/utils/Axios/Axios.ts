import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from "axios";
import { ElMessage } from "element-plus";
import router from "@/router";
import config from "@/config";
import store from "@/store";
import { getItem, setItem } from "@/utils/storage";

const axiosInatance = axios.create({
  baseURL: config.Host, // 基础路径
});

// 请求拦截器
const requestInterceptor = axiosInatance.interceptors.request.use(
  // 请求发起都会经过这里
  function (config: any) {
    const Token = getItem("Token");
    if (Token) {
      // 如果user数据和user.token为真，为有效得
      config.headers.Authorization = `Bearer ${Token}`; // 返回一个拼接好得有效的token值
    }
    return config;
    // config 本次请求的配置对象
  },
  function (err) {
    // console.log(err);
    // 请求出错（还没发出去）会经过这里
    return Promise.reject(err);
  }
);

// 响应拦截器
const responseInterceptor = axiosInatance.interceptors.response.use(
  (response: AxiosResponse): AxiosResponse => {
    // 2xx 范围内的状态码都会触发该函数。对响数据成功时调用。
    return response;
  },
  (err) => {
    if (err && err.response) {
      switch (err.response.status) {
        case 400:
          err.message = JSON.stringify(err.response.data.errors);
          break;
        case 401:
          err.message = "未授权，请登录";
          setItem("Token", "");
          setTimeout(() => {
            router.push("/");
          }, 500);
          break;
        case 403:
          err.message = "权限不足,拒绝访问";
          break;

        case 404:
          err.message = `请求地址不存在: ${err.response.config.url}`;
          break;

        case 408:
          err.message = "请求超时";
          break;

        case 500:
          err.message = "服务器内部错误";
          break;

        case 501:
          err.message = "服务未实现";
          break;

        case 502:
          err.message = "网关错误";
          break;

        case 503:
          err.message = "服务不可用";
          break;

        case 504:
          err.message = "网关超时";
          break;

        case 505:
          err.message = "HTTP版本不受支持";
          break;
        case 568:
          // todo
          err.message = err.response.data.errors;
          break;
        default:
          err.message = { ...err.response.data.errors };
      }
    }
    if (err.code === "ECONNABORTED" && err.message.indexOf("timeout") !== -1) {
      err.message = "请求超时,请重试";
    }
    ElMessage.error(err.message);
    return err;
  }
);

export default axiosInatance;
