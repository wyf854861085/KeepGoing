import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import axios from "axios"
import { ElMessage } from 'element-plus'
import * as ElIcons from '@element-plus/icons'
import { zhCn } from 'element-plus/es/locale'

import 'vant/lib/index.css';
import { Cell, CellGroup,Button,List,Col, Row,Divider,Field,Dialog ,Form,Notify,NavBar,Search,Tab, Tabs,Tag  } from 'vant';

const app = createApp(App);
for (const name in ElIcons) {
    app.component(name, (ElIcons as any)[name])
}

app.use(ElementPlus, { locale: zhCn });
app.use(store);
app.use(router);
app.use(Button);
app.use(List);
app.use(Cell);
app.use(Col);
app.use(Row);
app.use(Divider);
app.use(Dialog);
app.use(Form);
app.use(Notify);
app.use(NavBar);
app.use(Field);
app.use(CellGroup);
app.use(Search);
app.use(Tab);
app.use(Tabs);
app.use(Tag);

app.config.globalProperties.$axios = axios
app.config.globalProperties.$message = ElMessage
app.mount('#app')