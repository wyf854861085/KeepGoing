import { createStore } from 'vuex'
import { getItem, setItem } from "@/utils/storage";


export default createStore({
    state: {
        user: '',
        loading:false
    },
    mutations: {
        setToken(state, token) {
            state.user=token;
        },
        clearToken(state) {
            state.user='';
        },
        showLoading(state) {
            state.loading = true;
        },
        hideLoading(state) {
            state.loading = false;
        }
    },
    actions: {
    },
    modules: {
    }
})
