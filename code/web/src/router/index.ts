import {
  createRouter,
  createWebHashHistory,
  createWebHistory,
  RouteRecordRaw,
} from "vue-router";
import Home from "../Login.vue";

const routes: Array<RouteRecordRaw> = [
  { path: "/", name: "index", component: Home },
  {
    path: "/Manage",
    name: "Manage",
    component: () => import("@/views/Manage/index.vue"),
    children: [
      {
        path: "/RoleInfo",
        name: "RoleInfo",
        meta: { name: "角色管理", title: "角色管理" },
        component: () => import("@/views/Manage/RoleInfo.vue"),
      },
      {
        path: "/UserInfo",
        name: "UserInfo",
        meta: { name: "用户管理", title: "用户管理" },
        component: () => import("@/views/Manage/UserInfo.vue"),
      },
      {
        path: "/RoleAuthority",
        name: "RoleAuthority",
        meta: { name: "角色设置", title: "角色设置" },
        component: () => import("@/views/Manage/RoleAuthority.vue"),
      },
      {
        path: "/KeywordReply",
        name: "KeywordReply",
        meta: { name: "自动回复", title: "自动回复" },
        component: () => import("@/views/Manage/KeywordReply.vue"),
      },
      {
        path: "/Draft",
        name: "Draft",
        meta: { name: "素材管理", title: "素材管理" },
        component: () => import("@/views/Manage/Draft.vue"),
      },
    ],
  },
  {
    path: "/App",
    name: "App",
    component: () => import("@/views/App/index.vue"),
    children: [
      {
        path: "/Home",
        name: "Home",
        meta: { name: "App首页", title: "App首页" },
        component: () => import("@/views/App/Home.vue"),
      },
      {
        path: "/AppKeywordReply",
        name: "AppKeywordReply",
        meta: { name: "关键字管理", title: "关键字管理" },
        component: () => import("@/views/App/KeywordReply.vue"),
      },
      {
        path: "/AddArticle",
        name: "AddArticle",
        meta: { name: "添加文章", title: "添加文章" },
        component: () => import("@/views/App/AddArticle.vue"),
      },
    ],
  },
];

const router = createRouter({
  history: createWebHashHistory(process.env.BASE_URL),
  routes,
});

export default router;
